{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RankNTypes #-}
{-# OPTIONS_GHC -fno-cse #-}

module Main (main) where

import Control.Exception
import qualified Data.ByteString.Lazy as
  BL
import qualified Data.ByteString.Lazy.Char8 as
  BL.C8
import Data.Char as DC
import Data.Csv as
  CSV
import Data.List as
  DL
import qualified Data.Map as DM
import qualified Data.Set as DS
import qualified Data.Text as
  DT
import Data.Text.Read
import Data.Time as Time
import Data.Time as Time.Cal
import Data.Time.Format as TimeF
import Data.Typeable
import qualified Data.Vector as
  V
import Data.Vector.Storable (unsafeToForeignPtr,fromList)

import Foreign
import Foreign.C

import System.IO
import System.Locale (defaultTimeLocale)

data CMatrix = CMatrix {
  cMatrix_sizex::CUInt,
  cMatrix_sizey::CUInt,
  cMatrix_table::[[CDouble]]
} deriving (Show)

data CIntArr = CIntArr {
  cIntArr_size::CUInt,
  cIntArr_iarr::[CInt]
} deriving (Show)
 
data CDblArr = CDblArr {
  cDblArr_size::CUInt,
  cDblArr_darr::[CDouble]
} deriving (Show) 

data CTimeSeries = CTimeSeries {
  cTimeSeries_tm::CDblArr,
  cTimeSeries_dates::CIntArr,
  cTimeSeries_vols::CMatrix
} deriving (Show)

data COptionScenPrices = COptionScenPrices {
  cOptionScenPrices_contractPrice :: CDouble,
  cOptionScenPrices_interpolatedBaseVol :: CDouble,
  cOptionScenPrices_strike :: CDouble,
  cOptionScenPrices_tm :: CDouble,
  cOptionScenPrices_callOrPut :: CInt,
  cOptionScenPrices_stirOrNot :: CInt,
  cOptionScenPrices_optionPrice :: CDouble,
  cOptionScenPrices_dates::CIntArr,
  cOptionScenPrices_vols::CDblArr,
  cOptionScenPrices_scenContractPrices::CDblArr,
  cOptionScenPrices_scenOptionPrices::CDblArr
} deriving (Show)

instance Storable CMatrix where
    sizeOf _ = (16)
    alignment _ = 8
    peek ptr = do
      rsizex <- ((\hsc_ptr -> peekByteOff hsc_ptr 0)) ptr
      rsizey <- ((\hsc_ptr -> peekByteOff hsc_ptr 4)) ptr
      ptable <- ((\hsc_ptr -> peekByteOff hsc_ptr 8)) ptr
      arrOfP <- peekArray (fromIntegral rsizex) ptable
      let arr = Prelude.map (peekArray (fromIntegral rsizey)) arrOfP
      sarr <- sequence arr  
      return CMatrix {
        cMatrix_sizex=rsizex, 
        cMatrix_sizey=rsizey,
        cMatrix_table=sarr}

instance Storable CIntArr where
    sizeOf _ = (16)
    alignment _ = 8
    peek ptr = do
      rsize <- ((\hsc_ptr -> peekByteOff hsc_ptr 0)) ptr
      pInt <- ((\hsc_ptr -> peekByteOff hsc_ptr 8)) ptr
      riarr <- peekArray (fromIntegral rsize) pInt
      return CIntArr {cIntArr_size=rsize,cIntArr_iarr=riarr}

instance Storable CDblArr where
    sizeOf _ = (16)
    alignment _ = 8
    peek ptr = do
      rsize <- ((\hsc_ptr -> peekByteOff hsc_ptr 0)) ptr
      pDbl <- ((\hsc_ptr -> peekByteOff hsc_ptr 8)) ptr
      rdarr <- peekArray (fromIntegral rsize) pDbl
      return CDblArr {cDblArr_size=rsize,cDblArr_darr=rdarr}

instance Storable CTimeSeries where
    sizeOf _ = (24)
    alignment _ = 8
    peek ptr = do
      ptm <- ((\hsc_ptr -> peekByteOff hsc_ptr 0)) ptr
      htm <- peek ptm
      pdates <- ((\hsc_ptr -> peekByteOff hsc_ptr 8)) ptr
      hdates <- peek pdates
      pvols <- ((\hsc_ptr -> peekByteOff hsc_ptr 16)) ptr
      hvols <- peek pvols
      return CTimeSeries {
               cTimeSeries_tm=htm,
               cTimeSeries_dates=hdates,
               cTimeSeries_vols=hvols
             }

instance Storable COptionScenPrices where
    sizeOf _ = (80)
    alignment _ = 8
    peek ptr = do
      hcp <- ((\hsc_ptr -> peekByteOff hsc_ptr 0)) ptr
      hv <- ((\hsc_ptr -> peekByteOff hsc_ptr 40)) ptr
      hK <- ((\hsc_ptr -> peekByteOff hsc_ptr 8)) ptr
      htm <- ((\hsc_ptr -> peekByteOff hsc_ptr 16)) ptr
      hCP <- ((\hsc_ptr -> peekByteOff hsc_ptr 24)) ptr
      hS <- ((\hsc_ptr -> peekByteOff hsc_ptr 28)) ptr
      hop <- ((\hsc_ptr -> peekByteOff hsc_ptr 32)) ptr
      pdates <- ((\hsc_ptr -> peekByteOff hsc_ptr 48)) ptr
      hdates <- peek pdates
      pvols <- ((\hsc_ptr -> peekByteOff hsc_ptr 56)) ptr
      hvols <- peek pvols

      pscenP <- ((\hsc_ptr -> peekByteOff hsc_ptr 64)) ptr
      hscenP <- peek pscenP

      pscenOP <- ((\hsc_ptr -> peekByteOff hsc_ptr 72)) ptr
      hscenOP <- peek pscenOP

      return COptionScenPrices {
        cOptionScenPrices_contractPrice = hcp,
        cOptionScenPrices_interpolatedBaseVol = hv,
        cOptionScenPrices_strike = hK,
        cOptionScenPrices_tm = htm,
        cOptionScenPrices_callOrPut = hCP,
        cOptionScenPrices_stirOrNot = hS,
        cOptionScenPrices_optionPrice = hop,
        cOptionScenPrices_dates = hdates,
        cOptionScenPrices_vols = hvols,
        cOptionScenPrices_scenContractPrices = hscenP,
        cOptionScenPrices_scenOptionPrices = hscenOP
      }

data VolGrid = VolGrid {
  tm :: [Double],
  lnks :: [Double],
  vols :: [[Double]] } deriving (Eq,Show)

data FXScenarios = FXScenarios {
  fx_Date :: Time.Day,
  fx_scen :: Double } deriving (Show,Typeable)

data VarLosses = VarLosses {
  varL_inst :: DT.Text,
  varL_Date :: Time.Day,
  varL_Loss :: Double } deriving (Eq,Show,Typeable)

data ContractPrice = ContractPrice {
  cp_instr :: DT.Text,
  cp :: Double } deriving (Show)

data OptionParams = OptionParams {
  optionParams_id :: DT.Text,
  optionParams_valDt :: Time.Day,
  optionParams_matDt :: Time.Day,
  optionParams_type :: DT.Text,
  optionParams_und :: DT.Text,
  optionParams_K :: Double,
  optionParams_lotSize :: Double,
  optionParams_lots :: Double,
  optionParams_pathATM :: DT.Text,
  optionParams_pathGrid :: DT.Text,
  optionParams_curr :: DT.Text,
  optionParams_prodName :: DT.Text,
  optionParams_minPriceChange :: Double } deriving (Show)

data ATMVols = ATMVols {
  atmVols_tm :: [Double],
  atmVols_dt :: Time.Day,
  atmVols_vols :: [Double]} deriving (Show,Typeable)

data SimulationsPack = SimulationsPack {
  simulationsPack_arrVarlosses :: [VarLosses],
  simulationsPack_arrFxScenarios :: [FXScenarios],
  simulationsPack_arrContractPrices :: [ContractPrice],
  simulationsPack_arrOptionParams :: [OptionParams]} deriving (Show)

data SummaryResults = SummaryResults {
  summaryResults_id :: DT.Text,
  summaryResults_contractPrice :: CDouble,
  summaryResults_interpolatedBaseVol :: CDouble,
  summaryResults_strike :: CDouble,
  summaryResults_tm :: CDouble,
  summaryResults_callOrPut :: CInt,
  summaryResults_stirOrNot :: CInt,
  summaryResults_optionPrice :: CDouble,
  summaryResults_lotSize :: CDouble,
  summaryResults_es :: CDouble }

summaryHeader =
  "id," ++
  "contractPrice," ++
  "interpolatedBaseVol," ++
  "strike," ++
  "tm," ++
  "call/put," ++
  "stirOrNot," ++
  "optionPrice," ++
  "lotSize," ++
  "es"

summaryToString x =
  (DT.unpack $ summaryResults_id x) ++ "," ++
  (show $ summaryResults_contractPrice x) ++ "," ++
  (show $ summaryResults_interpolatedBaseVol x) ++ "," ++
  (show $ summaryResults_strike x) ++ "," ++
  (show $ summaryResults_tm x) ++ "," ++
  (show $ summaryResults_callOrPut x) ++ "," ++
  (show $ summaryResults_stirOrNot x) ++ "," ++
  (show $ summaryResults_optionPrice x) ++ "," ++
  (show $ summaryResults_lotSize x) ++ "," ++
  (show $ summaryResults_es x)

errorEvaluator t = case t of
  Left a -> error a
  Right a -> a

roundFnMinPriceChange minPriceChange x =
  (fromIntegral $ round $ x/minPriceChange)*minPriceChange

errorMsgEmtyMat1 = Left "Error : empty matrix"
errorMsgEmtyMat2 = Left "Error : (almost!) empty matrix"
errorMsgCoordsOrd = Left "Error : (x1,y1) has to be 'less' than (x2,y2)"
errorMsgCoordsBounds = (Left "Error : coords out of bounds")

type Mat a = V.Vector (V.Vector a)
type MatText = Mat DT.Text
data MatTSq = MatTSq { matTSq_get :: MatText } deriving (Eq,Show)

data Index1Mat = Index1Mat (MatTSq,Int) deriving (Show)
data Index2Mat = Index2Mat (MatTSq,Int) deriving (Show)
data IndexMat = IndexMat (MatTSq,Int,Int) deriving (Show)

checkWithinBounds1 m i = if V.length (matTSq_get m) <= i 
                         then Nothing
                         else Just $ Index1Mat (m,i)
                              
checkWithinBounds2 m i = let result = checkWithinBounds1 m 0
                         in
                           case result of
                             Nothing -> Nothing
                             _ ->
                               if (V.length (m' V.! 0)) <= i
                               then Nothing
                               else Just $ Index2Mat (m,i)
                                 where m' = matTSq_get m

extractCol (Index2Mat (arg,col)) lst =
    let arg' = matTSq_get arg in 
    if V.length arg' == 0
    then DL.reverse lst
    else
      extractCol (Index2Mat (MatTSq (V.tail arg'),col)) result
        where entry = (arg' V.! 0) V.! col
              result = entry:lst
              arg' = matTSq_get arg

extractRow (Index1Mat (arg,row)) = V.toList $ arg' V.! row
  where arg' = matTSq_get arg

extractSubset (x1,y1) (x2,y2) mat
  | xlen == 0 = errorMsgEmtyMat1
  | ylen == 0 = errorMsgEmtyMat2
  | (x1 > x2) || (y1 > y2) = errorMsgCoordsOrd
  | (x1 < 0) || (y1 < 0) || (x2 >= xlen) || (y2 >= ylen) =
      errorMsgCoordsBounds
  | otherwise = Right $ result
  where
    xlen = V.length mat
    ylen = V.length (mat V.! 0)
    
    cutOfRight = take2ndDim (y2+1) $ V.take (x2+1) mat
    result = drop2ndDim y1 $ V.drop x1 cutOfRight

    take2ndDim :: Int -> Mat a -> Mat a
    take2ndDim i = V.map $ V.take i
    drop2ndDim :: Int -> Mat a -> Mat a
    drop2ndDim i = V.map $ V.drop i

extractSubsetTSq (x1,y1) matTSq =
  if (V.length mat) == 0
  then Left "extractSubset : zero rows!"
  else      
    extractSubset (x1,y1) (xEnd,yEnd) mat
    where
      mat = matTSq_get matTSq
      xEnd = (V.length mat) - 1
      yEnd = (V.length (mat V.! 0)) -1

foreign import ccall "cMatrix_setRow"
  c_Matrix_setRow :: (Ptr CMatrix) -> (Ptr CDouble) -> CUInt -> CUInt -> IO (CInt)
foreign import ccall "cIntArr_set"
  c_IntArr_set :: (Ptr CIntArr) -> (Ptr CInt) -> CUInt -> IO (CInt)
foreign import ccall "cDblArr_set"
  c_DblArr_set :: (Ptr CDblArr) -> (Ptr CDouble) -> CUInt -> IO (CInt)

foreign import ccall "cMatrix_allocate"
  c_Matrix_allocate :: (Ptr CMatrix) -> CUInt -> CUInt -> IO (CInt)
foreign import ccall "cIntArr_allocate"
  c_IntArr_allocate :: (Ptr CIntArr) -> CUInt -> IO (CInt)
foreign import ccall "cDblArr_allocate"
  c_DblArr_allocate :: (Ptr CDblArr) -> CUInt -> IO (CInt)
foreign import ccall "cTimeSeries_allocate"
  c_TimeSeries_allocate :: (Ptr CTimeSeries) -> IO (CInt)
foreign import ccall "cOptionScenPrices_allocate"
  c_OptionScenPrices_allocate :: (Ptr COptionScenPrices) -> IO (CInt)

foreign import ccall "&cMatrix_free"
  c_funptr_Matrix_free :: FunPtr (Ptr CMatrix -> IO ())
foreign import ccall "&cIntArr_free"
  c_funptr_IntArr_free :: FunPtr (Ptr CIntArr -> IO ())
foreign import ccall "&cDblArr_free"
  c_funptr_DblArr_free :: FunPtr (Ptr CDblArr -> IO ())
foreign import ccall "&cTimeSeries_free"
  c_funptr_TimeSeries_free :: FunPtr (Ptr CTimeSeries -> IO ())
foreign import ccall "&cOptionScenPrices_free"
  c_funptr_OptionScenPrices_free :: FunPtr (Ptr COptionScenPrices -> IO ())

foreign import ccall "cProcessATMVols"
  c_processATMVols :: (Ptr CIntArr) -> (Ptr CDblArr) ->
                      (Ptr CMatrix) -> (Ptr CTimeSeries) ->
                      IO()

foreign import ccall "cCalculateOptionScenPrices"
  c_calculateOptionScenPrices :: CDouble -> CDouble -> CDouble ->
    CInt -> CInt ->
    (Ptr CIntArr) -> (Ptr CDblArr) ->
    (Ptr CIntArr) -> (Ptr CDblArr) ->
    (Ptr CMatrix) -> (Ptr CDblArr) ->
    (Ptr CDblArr) -> (Ptr CMatrix) ->
    (Ptr COptionScenPrices) -> IO ()

c_doWithPtr1 fp fun = do
  status <- withForeignPtr fp (\p -> do
    status <- fun p
    return status)
  return status

c_doWithPtr2 fp1 fp2 fun = do
  status <- withForeignPtr fp1 (\p1 -> do
    status <- withForeignPtr fp2 (\p2 -> do
      status <- fun p1 p2
      return status)
    return status)
  return status

c_doWithPtr4 fp1 fp2 fp3 fp4 fun = do
  status <- withForeignPtr fp1 (\p1 -> do
    status <- withForeignPtr fp2 (\p2 -> do
      status <- withForeignPtr fp3 (\p3 -> do
        status <- withForeignPtr fp4 (\p4 -> do
          status <- fun p1 p2 p3 p4
          return status)
        return status)
      return status)
    return status)
  return status

c_doWithPtr9 fp1 fp2 fp3 fp4 fp5 fp6 fp7 fp8 fp9 fun = do
  status <- withForeignPtr fp1 (\p1 -> do
    status <- withForeignPtr fp2 (\p2 -> do
      status <- withForeignPtr fp3 (\p3 -> do
        status <- withForeignPtr fp4 (\p4 -> do
          status <- withForeignPtr fp5 (\p5 -> do
            status <- withForeignPtr fp6 (\p6 -> do
              status <- withForeignPtr fp7 (\p7 -> do
                status <- withForeignPtr fp8 (\p8 -> do
                  status <- withForeignPtr fp9 (\p9 -> do  
                    status <- fun p1 p2 p3 p4 p5 p6 p7 p8 p9
                    return status)
                  return status)
                return status)
              return status)
            return status)
          return status)
        return status)
      return status)
    return status)
  return status

c_allocate alloc free = do
  fp <- mallocForeignPtr
  status <- c_doWithPtr1 fp alloc
  addForeignPtrFinalizer free fp
  return fp

c_get fp = c_doWithPtr1 fp peek

c_createMatrix rows columns = do
  volGridPtr <- c_allocate
    (\x -> c_Matrix_allocate x rows columns)
    c_funptr_Matrix_free
  return volGridPtr

c_createIntArr arrLen = do
  iArrPtr <- c_allocate
    (\x -> c_IntArr_allocate x arrLen)
    c_funptr_IntArr_free
  return iArrPtr

c_createDblArr arrLen = do
  dArrPtr <- c_allocate
    (\x -> c_DblArr_allocate x arrLen)
    c_funptr_DblArr_free
  return dArrPtr

c_createTimeSeries =
  c_allocate c_TimeSeries_allocate c_funptr_TimeSeries_free

c_createOptionScenPrices =
  c_allocate c_OptionScenPrices_allocate
             c_funptr_OptionScenPrices_free

c_setRowOfCMatrix matPtr rowPtr row len = c_doWithPtr2
  matPtr rowPtr (\x y -> c_Matrix_setRow x y row len)

c_setCIntArr toPtr fromPtr len = c_doWithPtr2
  toPtr fromPtr (\x y -> c_IntArr_set x y len)

c_setCDblArr toPtr fromPtr len = c_doWithPtr2
  toPtr fromPtr (\x y -> c_DblArr_set x y len)

processATMVols fp_dts fp_tm fp_vols fp_results =
  c_doWithPtr4 fp_dts fp_tm fp_vols fp_results c_processATMVols 

calculateOptionScenPrices futurePrice strike timeToMat
 callOrPut stirOrNot
 fpScenDates fpScenFutPrices
 fpAtmScenVol_dts fpAtmScenVol_tm 
 fpAtmScenVol_vols fpVolGrid_tm
 fpVolGrid_lnks fpVolGrid_vols 
 fpResults = c_doWithPtr9 fpScenDates fpScenFutPrices
   fpAtmScenVol_dts fpAtmScenVol_tm 
   fpAtmScenVol_vols fpVolGrid_tm
   fpVolGrid_lnks fpVolGrid_vols 
   fpResults (c_calculateOptionScenPrices futurePrice strike
              timeToMat callOrPut stirOrNot)

comma = fromIntegral (DC.ord ',')
space = fromIntegral (DC.ord ' ')
newline = fromIntegral (DC.ord '\n')
rightslash = fromIntegral (DC.ord '/')
bytenull = BL.C8.pack ""
commaB = BL.C8.pack ","
newlineB = BL.C8.pack "\n"

removeSpacesAroundComma arg = BL.concat [restitched2,newlineB] where
  splitString1 = BL.split comma arg
  remSpaceComma =
    map (\x -> removeIdenFrontBack x space) splitString1
  restitched1 = BL.intercalate commaB remSpaceComma
  splitString2 = BL.split newline restitched1
  splitString3 = filter (/=bytenull) splitString2
  remSpaceNewline =
    map (\x -> removeIdenFrontBack x space) splitString3
  restitched2 = BL.intercalate newlineB remSpaceNewline

  removeIdenFromFront arg iden
    | arg == bytenull = arg
    | BL.head arg == iden = removeIdenFromFront (BL.tail arg) iden
    | otherwise = arg

  removeIdenFromBack arg iden =
    (BL.reverse $ removeIdenFromFront (BL.reverse arg) iden)

  removeIdenFrontBack arg iden =
    removeIdenFromBack (removeIdenFromFront arg iden) iden

errorMsgParseCsv =
  (Left $ "Parse error : Only one or less lines"
   ++" read. To parse csv you need more than 1 line")
errorMsgDiffEntries =
  (Left $ "Parse error : Number of entries per line varies")
errorMsgNoNewLn =
  (Left "string should not contain newlines")

errorMsgTToD = Left "Error converting text to double"
errorMsgGridExt = Left "Error extracting grid"

inputColTm = 2
inputColFirstMoneyness = inputColTm + 1
inputTmIden = DT.pack "Tm"
inputVarLossColOfInstr = 1

convertDayToExcelInt d = (fromIntegral (Time.Cal.diffDays d ((Time.readTime defaultTimeLocale "%Y%m%d" "19000101")))) + 2
convertExcelIntToDay i = Time.Cal.addDays (toInteger (i-2)) (Time.readTime defaultTimeLocale "%Y%m%d" "19000101")

convertTextArrayToNum = (sequence . parseDouble . textArrayToEithArr)
  where
    textArrayToEithArr arr = DL.map rational arr
    parseDouble arrEither = DL.map extractDouble arrEither 
    extractDouble (Left a) = errorMsgTToD
    extractDouble (Right (x,t)) = if t /= DT.pack ""
                                  then errorMsgTToD
                                  else Right x

convertTextMatToNum arg = eLL
  where
    vecLst = V.map V.toList arg
    lstEith = 
      V.map convertTextArrayToNum vecLst
    eLL = sequence $ V.toList lstEith

extById _ all [] = Right []
extById arg all from = let
  csvParsed = CSV.decode CSV.NoHeader (DL.head from)
  callOnRemaining = extById arg all (DL.tail from)
  in case csvParsed of
    Left a -> Left a
    Right a -> if V.length a /= 1 then errorMsgNoNewLn else
      case callOnRemaining of
        Left b -> Left b
        Right b ->
          if all == True || a V.! 0 V.! 0 == arg
          then Right $ [DL.head from] ++ b
          else Right $ [] ++ b

extractById str identifier
  | (DL.length $ filter (/=bytenull) $ BL.split newline str) <= 1 = errorMsgParseCsv
  | otherwise =
    let
      brokenUp = filter (/=bytenull) $ BL.split newline str
      header = DL.head brokenUp
      tail = DL.tail brokenUp
      parseRem = if identifier == (DT.pack "*")
                 then extById identifier True tail
                 else extById identifier False tail
    in case parseRem of
      Left a -> Left a
      Right a ->
        let  
          str' = BL.C8.unlines $ header:a
          strUnpack = BL.C8.unpack str'
          strRepack = BL.C8.pack strUnpack
          csvParsed = (CSV.decode CSV.NoHeader strRepack)
        in
         if a == []
         then (Left $ "reading grid error : no matchs found for " ++ (DT.unpack identifier))
         else csvParsed

gridCheckSameNumberOfCols arg
  | V.length arg <= 1 = True
  | otherwise =
    let numColsInFirstRow = V.length $ arg V.! 0
    in gridCheck (V.tail arg) numColsInFirstRow
    where
      gridCheck arg expectedRows
        | V.length arg == 0 = True
        | otherwise =
          if (V.length $ arg V.! 0) /= expectedRows
          then False
          else gridCheck (V.tail arg) expectedRows       

parseGrid str dt =
  let arg = removeSpacesAroundComma str
      argUnpack = BL.C8.unpack arg
      argRepack = BL.C8.pack argUnpack in
  case extractById argRepack dt of
    Left str -> Left str
    Right v -> if (gridCheckSameNumberOfCols v) == True
               then Right (MatTSq v)
               else errorMsgDiffEntries

readFileAndRemoveHeader file = do
  readData <- BL.readFile file
  let result = case parseGrid readData (DT.pack "*") of
        Left a -> Left $ "Error reading " ++ file ++ " --> " ++ a
        Right b -> Right $ MatTSq (textMatNoHead)
          where textMat = matTSq_get b
                textMatNoHead = V.tail textMat
  return result

parseVarLosses arg = case arg of
  Left a -> Left a
  Right b -> result where

    inputVarLossColOfDt = 3
    inputVarLossColOfLoses = 4
    
    inst = checkWithinBounds2 b inputVarLossColOfInstr
    dts = checkWithinBounds2 b inputVarLossColOfDt
    loss = checkWithinBounds2 b inputVarLossColOfLoses

    extractor x = case x of
      Nothing -> Left "Varlosses parsing error. Field selector incorrect."
      Just b  -> Right $ extractCol b []

    arrInst = extractor inst
    arrDts = extractor dts
    arrLoss = extractor loss

    collated = sequence [arrInst,arrDts,arrLoss]

    createArrVecs a b c i = VarLosses {
      varL_inst = a !! i,
      varL_Date = Time.readTime defaultTimeLocale "%d-%b-%y" $ DT.unpack (b !! i),
      varL_Loss = c !! i }
    
    result = case collated of
      Left a -> Left a
      Right a -> 
        let elossA = convertTextArrayToNum (a !! 2) in
        case elossA of
          Left _ -> Left "Varlosses parsing error when converting one of the columns from text to numbers"
          Right lstLosses ->
            Right $ map (createArrVecs (a !! 0) (a !! 1) lstLosses) [0..(l-1)]
            where  
              l = length $ a !! 0

parseTextDoublePair arg colTx colDbl fn = case arg of
  Left a -> Left a
  Right b -> result where
    
    tx = checkWithinBounds2 b colTx
    dbl = checkWithinBounds2 b colDbl

    extractor x = case x of
      Nothing -> Left "Generic parsing error. Field selector incorrect."
      Just b  -> Right $ extractCol b []

    arrTx = extractor tx
    arrDbl = extractor dbl

    collated = sequence [arrTx, arrDbl]

    result = case collated of
      Left a -> Left a
      Right a ->
        let dblA = convertTextArrayToNum (a !! 1) in
        case dblA of
          Left _ -> Left "Generic parsing error when converting one of the columns from text to numbers"
          Right lstDbl ->
            Right $ map (fn (a !! 0) lstDbl) [0..(l-1)]
          where 
            l = length $ a !! 0

parseFXScenarios arg =
  case parseTextDoublePair arg inputFXScenDt inputFXScenFX
    createArrFX of
    Left a -> Left $ "Error parsing FX Scenarios --> " ++ a
    Right a -> Right a
  where
    createArrFX a b i = FXScenarios {
      fx_Date = Time.readTime defaultTimeLocale "%Y-%m-%d" $ DT.unpack (a !! i),
      fx_scen = b !! i
    }
    inputFXScenDt = 0
    inputFXScenFX = 2

checkErrorsGeneric (Left a) _ = do return (Left a)
checkErrorsGeneric (Right []) _ = do
  return $ Left "Empty list passed into date checker"
checkErrorsGeneric i@(Right lst@(x:xs)) getDate = do
  let
    lstIOExcChk = map
      (\x -> (try (evaluate (getDate x)))
             ::IO(Either SomeException Time.Day))
      lst
  lstExcChk <- sequence lstIOExcChk
  let
    anyErrors = sequence lstExcChk
    result = case anyErrors of
      Left a -> Left $ "Error parsing " ++ (show (typeOf (head lst))) ++ " due to invalid date : " ++ show a
      _ -> i
  return result

checkErrorsATMVols arg = checkErrorsGeneric arg atmVols_dt  
checkErrorsFXScenarios arg = checkErrorsGeneric arg fx_Date  
checkErrorsVarLosses arg = checkErrorsGeneric arg varL_Date  
checkErrorsOptionParams (Left a) = do return (Left a)
checkErrorsOptionParams i@(Right lst) = do
  let
    lstIOExcChk_valDt = map
      (\x -> (try (evaluate (optionParams_valDt x)))
             ::IO(Either SomeException Time.Day))
      lst
  lstExcChk_valDt <- sequence lstIOExcChk_valDt    
  let 
    lstIOExcChk_matDt = map
      (\x -> (try (evaluate (optionParams_matDt x)))
             ::IO(Either SomeException Time.Day))
      lst
  lstExcChk_matDt <- sequence lstIOExcChk_matDt    

  let
    anyErrors1 = sequence lstExcChk_valDt
    anyErrors2 = sequence lstExcChk_matDt
    anyErrors = sequence [anyErrors1,anyErrors2]
    result = case anyErrors of
      Left a -> Left $ "Error parsing option parameters due to invalid date --> " ++ show a
      _ -> i
  return result

parseContractPrices arg =
  parseTextDoublePair arg inputPriceCT inputPriceP
    createArrPr
  where
    createArrPr a b i = ContractPrice {
      cp_instr = a !! i,
      cp = b !! i
    }
    inputPriceCT = 1
    inputPriceP = 2

parseOptionParams arg = case arg of
  Left a -> Left a
  Right b -> result where

    col_id = 0
    col_valDt = 7
    col_long_lots = 10;
    col_short_lots = 11;
    col_underlying = 27;
    col_prod_name = 33;
    col_K = 37;
    col_call_or_put = 38;
    col_lotSize = 39;
    col_matDt = 41;
    col_currency = 49;
    col_pathATM = 68;
    col_pathGrid = 69;
    col_minPriceChange = 70;
    
    c_col_id = checkWithinBounds2 b col_id
    c_col_valDt = checkWithinBounds2 b col_valDt
    c_col_matDt = checkWithinBounds2 b col_matDt
    c_col_call_or_put = checkWithinBounds2 b col_call_or_put
    c_col_underlying = checkWithinBounds2 b col_underlying
    c_col_K = checkWithinBounds2 b col_K
    c_col_lotSize = checkWithinBounds2 b col_lotSize
    c_col_long_lots = checkWithinBounds2 b col_long_lots
    c_col_short_lots = checkWithinBounds2 b col_short_lots
    c_col_pathATM = checkWithinBounds2 b col_pathATM
    c_col_pathGrid = checkWithinBounds2 b col_pathGrid
    c_col_currency = checkWithinBounds2 b col_currency
    c_col_prod_name = checkWithinBounds2 b col_prod_name
    c_col_minPriceChange = checkWithinBounds2 b col_minPriceChange
    
    extractor x = case x of
      Nothing -> Left "Option parsing error. Field selector incorrect."
      Just b  -> Right $ extractCol b []

    arr_id = extractor c_col_id
    arr_valDt = extractor c_col_valDt
    arr_matDt = extractor c_col_matDt
    arr_call_or_put = extractor c_col_call_or_put
    arr_underlying = extractor c_col_underlying
    arr_K = extractor c_col_K
    arr_lotSize = extractor c_col_lotSize
    arr_long_lots = extractor c_col_long_lots
    arr_short_lots = extractor c_col_short_lots
    arr_pathATM = extractor c_col_pathATM
    arr_pathGrid = extractor c_col_pathGrid
    arr_curr = extractor c_col_currency
    arr_prodName = extractor c_col_prod_name
    arr_minPriceChange = extractor c_col_minPriceChange
    
    collated = sequence
               [arr_id, -- 0
                arr_valDt,
                arr_matDt, -- 2
                arr_call_or_put,
                arr_underlying, -- 4
                arr_K,
                arr_lotSize, -- 6
                arr_long_lots,
                arr_pathATM, -- 8
                arr_pathGrid,
                arr_curr, -- 10
                arr_short_lots,
                arr_prodName, -- 12
                arr_minPriceChange
               ]

    createArrVecs a_id a_valDt a_matDt a_type a_und a_k
      a_lotSize a_long_lots a_pathATM a_pathGrid a_curr
      a_short_lots arr_prodName arr_minPriceChange i = OptionParams {
        optionParams_id = a_id !! i,
        optionParams_valDt = Time.readTime defaultTimeLocale "%d-%b-%y" $ DT.unpack (a_valDt !! i),
        optionParams_matDt = Time.readTime defaultTimeLocale "%d-%b-%y" $ DT.unpack (a_matDt !! i),
        optionParams_type = DT.toLower $ a_type !! i,
        optionParams_und = a_und !! i,
        optionParams_K = a_k !! i,
        optionParams_lotSize = a_lotSize !! i,
        optionParams_lots = (a_long_lots !! i) - (a_short_lots !! i),
        optionParams_pathATM = a_pathATM !! i,
        optionParams_pathGrid = a_pathGrid !! i,
        optionParams_curr = DT.toLower $ a_curr !! i,
        optionParams_prodName = DT.toLower $ arr_prodName !! i,
        optionParams_minPriceChange = arr_minPriceChange !! i
        }

    result = case collated of
      Left a -> Left a
      Right a ->
        let dblK = convertTextArrayToNum (a !! 5 )
            dblMult = convertTextArrayToNum (a !! 6 )
            dblLongLots = convertTextArrayToNum (a !! 7 )
            dblShortLots = convertTextArrayToNum (a !! 11 )
            dblMinPriceChange = convertTextArrayToNum (a !! 13 )
            
            checkForErr = sequence [dblK, dblMult,
                                    dblLongLots, dblShortLots,
                                    dblMinPriceChange]
        in case checkForErr of
          Left e -> Left $ "Option parameters parsing error when converting one of the columns from text to numbers --> " ++ e
          Right lstOfArrs ->
            Right $ map (createArrVecs
              (a !! 0)
              (a !! 1)
              (a !! 2)
              (a !! 3)
              (a !! 4)
              (lstOfArrs !! 0)
              (lstOfArrs !! 1)
              (lstOfArrs !! 2) 
              (a !! 8)
              (a !! 9)
              (a !! 10)
              (lstOfArrs !! 3)
              (a !! 12)
              (lstOfArrs !! 4)) [0..(l-1)]
            where
              l = length $ a !! 0

parseATMVols (Left a) = (Left a)
parseATMVols (Right m) = result
  where
    firstColOfTm = 3
    colOfDt = 0
    rowOfTm = 0 

    atLeast2Rows = checkWithinBounds1 m (rowOfTm + 1)
    atLeast4Cols = checkWithinBounds2 m firstColOfTm
    
    arrTmText = case (checkWithinBounds1 m rowOfTm) of
      Nothing -> []
      Just row -> drop firstColOfTm $ extractRow row
    arrDtText = case (checkWithinBounds2 m colOfDt) of
      Nothing -> []
      Just col -> drop (rowOfTm + 1) $ extractCol col []
    arrVolsText = extractSubsetTSq (1,firstColOfTm) m

    eTm = convertTextArrayToNum arrTmText
    arrDt::[Time.Day] = 
      map (Time.readTime defaultTimeLocale "%Y%m%d") 
        (map DT.unpack arrDtText)
    eArrVols = case arrVolsText of 
      Left a -> Left a
      Right vols -> convertTextMatToNum vols

    createATMVolData _ [] _ lst = reverse lst 
    createATMVolData 
      tmArr dtsArr@(d:ds) arrVols@(v:vs) lst = let
        atm = ATMVols {
          atmVols_tm = tmArr,
          atmVols_dt = d,
          atmVols_vols = v}
        in createATMVolData tmArr ds vs (atm:lst)
      
    result = case atLeast2Rows of
      Nothing -> Left "Error parsing insufficient rows in ATM vols"
      Just _ -> result2
    result2 = case atLeast4Cols of
      Nothing -> Left "Error parsing insufficient cols in ATM vols"
      Just _ -> result3
    result3 = case arrTmText of
      [] -> Left "Error parsing ATM Vols : Time to mat array is empty"
      _ -> result4
    result4 = case arrDtText of
      [] -> Left "Error parsing ATM Vols : date array is empty"
      _ -> result5
    result5 = case eArrVols of 
      Left a -> Left $ "ATM Vols : error parsing vols --> " ++ a
      Right arrVols -> result6 arrVols
    result6 arrVols = case eTm of
      Left a -> Left $ "ATM Vols : error parsing time to mats --> " 
                       ++ a 
      Right tmArr -> 
        if length tmArr == 0 
          then Left $ "ATM Vols : error parsing, time to mats arr is empty"
          else result7 arrVols tmArr
    result7 arrVols tmArr = case arrDt of
      [] -> Left "ATM Vols : error parsing, date array is empty"
      dtsArr -> result8 arrVols tmArr dtsArr
    result8 arrVols tmArr dtsArr = 
      if (length tmArr /= length (arrVols !! 0))
      then Left $ "ATM Vols : error parsing, time to maturity array"
        ++ " does not match number of vol entries per day"
      else result9 arrVols tmArr dtsArr
    result9 arrVols tmArr dtsArr = 
      if (length dtsArr /= length arrVols)
      then Left $ "ATM Vols : error parsing, mismatch between"
        ++ " number of days and number of vols"
      else Right $ createATMVolData tmArr dtsArr arrVols []

parseVolGrid g =
  case g of
    Left a -> Left a
    Right b ->
        if (V.length (matTSq_get b) < 2)
        then
          (Left "Error : not enough rows in grid. Must be atleast 2.")
        else
          case (checkWithinBounds2 b inputColFirstMoneyness) of
            Nothing -> (Left "Error : not enough columns in the grid")
            Just x ->
              let b' = matTSq_get b in
              if (((b' V.! 0) V.! inputColTm) /= inputTmIden)
              then
                (Left "Error : header for timeToMat should be Tm")
              else
                extractGrid b 
                  inputColFirstMoneyness
                  inputColTm
                  inputTmIden
                  x

extractGrid b colOfHGrid i_colTm2 iden x = let
  rowMoneynessText = case (checkWithinBounds1 b 0) of
    Nothing -> []
    Just row -> drop colOfHGrid $ extractRow row
  rowMoneyness = convertTextArrayToNum rowMoneynessText
  colTmText = case (checkWithinBounds2 b i_colTm2) of 
    Nothing -> []
    Just col -> tail $ extractCol col []
  colTm = convertTextArrayToNum colTmText
  volsText = extractSubsetTSq (1,colOfHGrid) b
  volsD = case volsText of 
    Left a -> Left a
    Right vols -> convertTextMatToNum vols
  in
    if colTmText == [] || rowMoneynessText == []
    then Left "unable to parse moneyness/time to maturities of vol grid"
    else getVolGrid rowMoneyness colTm volsD

getVolGrid money tm vols =
  case money of
    Left al -> errorMsgGridExt
    Right ar ->
      case tm of
        Left bl -> errorMsgGridExt
        Right br ->
          case vols of
            Left cl -> errorMsgGridExt
            Right cr ->
              Right $ VolGrid { tm = br, lnks = ar, vols = cr }

readVolGrid fileGrid dt = do
  readData <- BL.readFile fileGrid
  let parsed = parseGrid readData dt
      result = case parsed of
        Left a -> Left $ "Error reading volGrid --> " ++ a
        Right a -> Right a
  return result

readVolATM fileGrid = do
  readData <- BL.readFile fileGrid
  let parsed = parseGrid readData (DT.pack "*")
      result = case parsed of
        Left a -> Left $ "Error reading ATM vols --> " ++ a
        Right a -> Right a
  return result

c_moveList_into_c [] _ _ _ =
  return $ Left "Error creating and setting" 
c_moveList_into_c lst@(_:_) cfn c_create c_set = do
  let (from,_,len) =
        unsafeToForeignPtr $ fromList $ map cfn lst
      ilen = fromIntegral len  
  to <- c_create ilen
  status <- c_set to from ilen
  return $ if status == -1
           then Left "List Error creating and setting 2"
           else Right to

c_moveArray_into_c [] _ _ _ =
  return $ Left "Array : Error creating and setting 1"
c_moveArray_into_c [[]] _ _ _ =
  return $ Left "Array : Error creating and setting 2"
c_moveArray_into_c lstLst cfn c_create c_set = do
  let rows = length lstLst
      cols = length (head lstLst)
      lstOfS = map fromList $ map (\x -> (map cfn x)) lstLst
      lstOfP = map (\(p,_,len) -> p) $
                 map unsafeToForeignPtr lstOfS
      intPPair = zip [0..(rows-1)] lstOfP
  pTo <- c_create (fromIntegral rows) (fromIntegral cols)    
  let setRowsIO = map (\(r,pFrom) -> c_set pTo pFrom (fromIntegral r)
                    (fromIntegral cols))
                  intPPair
  statusArr <- sequence(setRowsIO)
  let statusFail = filter (==(-1)) statusArr
  return $ if length statusFail > 0
           then Left "Array : Error creating and setting 3"
           else Right pTo

c_createScaledATMReturns atmVols = do
  let tms = atmVols_tm (head atmVols)
      dts = map atmVols_dt atmVols
      vols = map atmVols_vols atmVols
      dtsInt = map (fromIntegral . convertDayToExcelInt) dts
  
  epDt <- c_moveList_into_c dtsInt CInt c_createIntArr
            c_setCIntArr
  let pDt = errorEvaluator epDt

  epTm <- c_moveList_into_c tms CDouble c_createDblArr
            c_setCDblArr
  let pTm = errorEvaluator epTm

  epVols <- c_moveArray_into_c vols CDouble c_createMatrix c_setRowOfCMatrix
  let pVols = errorEvaluator epVols

  pResults <- c_createTimeSeries
  t <- processATMVols pDt pTm pVols pResults
  results <- c_get pResults
  
  return results where

readInFiles locInstrumentVarLosses
            locScenarioFX
            locResults
            locOptionPosition = do
              
  varlosses <- readFileAndRemoveHeader locInstrumentVarLosses
  fxScenarios <- readFileAndRemoveHeader locScenarioFX
  contractPrices <- readFileAndRemoveHeader locResults
  optionParams <- readFileAndRemoveHeader locOptionPosition
  
  evaluate(errorEvaluator(varlosses))
  evaluate(errorEvaluator(fxScenarios))
  evaluate(errorEvaluator(contractPrices))
  evaluate(errorEvaluator(optionParams))

  let uncheckedParsedLosses = parseVarLosses varlosses
      uncheckedParsedFX = parseFXScenarios fxScenarios
      parsedCTP = parseContractPrices contractPrices
      uncheckedParsedOptionParams =
        parseOptionParams optionParams
  parsedLosses <- checkErrorsVarLosses uncheckedParsedLosses
  parsedFXChecked <- checkErrorsFXScenarios uncheckedParsedFX
  parsedOptionParamsChecked <- checkErrorsOptionParams
    uncheckedParsedOptionParams

  let arrLosses = DL.nub $ errorEvaluator(parsedLosses)
      arrCTP = errorEvaluator(parsedCTP)
      arrFX = errorEvaluator(parsedFXChecked)
      arrOptionParams = errorEvaluator(parsedOptionParamsChecked)

  evaluate(arrLosses,arrCTP,arrFX,arrOptionParams)
  
  return SimulationsPack {
      simulationsPack_arrVarlosses = arrLosses,
      simulationsPack_arrFxScenarios = arrFX,
      simulationsPack_arrContractPrices = arrCTP,
      simulationsPack_arrOptionParams = arrOptionParams
    }

performOptionScenPricesCalc
  contractPrice strike timeToMat callOrPut stirOrNot
  scen atmScenReturns volGrid = do
    let 
        scenDates = map (\(x,_) -> x) scen
        scenFutPrices = map (\(_,y) -> y) scen
        atmScenVol_tm = cTimeSeries_tm atmScenReturns
        atmScenVol_dts = cTimeSeries_dates atmScenReturns
        atmScenVol_vols = cTimeSeries_vols atmScenReturns
        volGrid_tm = tm volGrid
        volGrid_lnks = lnks volGrid
        volGrid_vols = vols volGrid
        scenDatesInt = map fromIntegral scenDates
        
    eScenDates <- c_moveList_into_c scenDatesInt CInt
      c_createIntArr c_setCIntArr
    eScenFutPrices <- c_moveList_into_c scenFutPrices CDouble
      c_createDblArr c_setCDblArr
    eAtmScenVol_tm <- c_moveList_into_c (cDblArr_darr atmScenVol_tm)
      (\x -> x) c_createDblArr c_setCDblArr
    eAtmScenVol_dts <- c_moveList_into_c (cIntArr_iarr atmScenVol_dts)
      (\x -> x) c_createIntArr c_setCIntArr
    eAtmScenVol_vols <-c_moveArray_into_c (cMatrix_table atmScenVol_vols)
      (\x -> x) c_createMatrix c_setRowOfCMatrix
    eVolGrid_tm <- c_moveList_into_c volGrid_tm CDouble
      c_createDblArr c_setCDblArr
    eVolGrid_lnks <- c_moveList_into_c volGrid_lnks CDouble
      c_createDblArr c_setCDblArr
    eVolGrid_vols <- c_moveArray_into_c volGrid_vols CDouble
      c_createMatrix c_setRowOfCMatrix

    let cScenDates = errorEvaluator eScenDates
        cScenFutPrices = errorEvaluator eScenFutPrices
        cAtmScenVol_tm = errorEvaluator eAtmScenVol_tm
        cAtmScenVol_dts = errorEvaluator eAtmScenVol_dts
        cAtmScenVol_vols = errorEvaluator eAtmScenVol_vols
        cVolGrid_tm = errorEvaluator eVolGrid_tm
        cVolGrid_lnks = errorEvaluator eVolGrid_lnks
        cVolGrid_vols = errorEvaluator eVolGrid_vols

    pResults <- c_createOptionScenPrices
    
    t <- calculateOptionScenPrices
           (CDouble contractPrice) (CDouble strike) (CDouble timeToMat)
           (CInt (fromIntegral callOrPut)) (CInt (fromIntegral stirOrNot))
           cScenDates cScenFutPrices
           cAtmScenVol_dts cAtmScenVol_tm
           cAtmScenVol_vols cVolGrid_tm
           cVolGrid_lnks cVolGrid_vols
           pResults
           
    res <- c_get pResults
    return res

runSim inputFile = do
  f <- (BL.readFile inputFile)
  let args = map BL.C8.unpack $ BL.split newline f
      resultsDir = args !! 4
  simulation <- readInFiles
    (args !! 0) 
    (args !! 1) 
    (args !! 2) 
    (args !! 3) 

  let
    arrLosses = simulationsPack_arrVarlosses simulation
    arrFX = simulationsPack_arrFxScenarios simulation
    arrCTP = simulationsPack_arrContractPrices simulation
    arrOptionParams = simulationsPack_arrOptionParams simulation
    numOptions = length arrOptionParams
    optionIndex = 0

  print "Loading volatilities"
  
  volGridAndATMMap <- populateVolMap arrOptionParams optionIndex
    numOptions DM.empty

  let volGridAndATMLst = DM.toList volGridAndATMMap
      (volId,volP) = unzip volGridAndATMLst
      namesOfATM = map (\(_,y) -> y) volId
      (volGrid,volsATMs) = unzip volP

  print "Creating scaled vol returns"

  volsATMScen <- sequence $ map c_createScaledATMReturns volsATMs

  saveScaledRets namesOfATM resultsDir volsATMScen

  let volGridAndATMScenRets  = DM.fromList $
        zip volId $ zip volGrid volsATMScen

  print "Commencing option pricing"
      
  let                
    optionIndex = 0
  arrSummary <- loopOverOptions arrOptionParams
         optionIndex numOptions arrLosses arrFX arrCTP resultsDir
         volGridAndATMScenRets []
  printToFile_Summary resultsDir arrSummary
  return ()
  where
    loopOverOptions arrOptionParams optionIndex numOptions
      arrLosses arrFX arrCTP resultsDir volMap arrSummary = do
      if optionIndex == numOptions
      then return arrSummary
      else do
        putStrLn $ "Option " ++ (show (optionIndex + 1))

        resultsOptionSims <- performSimForOneOption
          arrOptionParams optionIndex numOptions arrLosses arrFX
          arrCTP resultsDir volMap
        
        loopOverOptions arrOptionParams (optionIndex+1) 
          numOptions arrLosses arrFX arrCTP resultsDir volMap
          (arrSummary++[resultsOptionSims])
    
    saveScaledRets [] resultsDir [] = do return ()
    saveScaledRets (n:ns) resultsDir (x:xs) = do
        let name = BL.C8.unpack $ last $ 
              BL.split rightslash (BL.C8.pack n)

        printToFile_CTimeSeries resultsDir name
          (cDblArr_darr (cTimeSeries_tm x))
          (cIntArr_iarr (cTimeSeries_dates x))
          (cMatrix_table (cTimeSeries_vols x))
        
        saveScaledRets ns resultsDir xs

populateVolMap arrOptionParams optionIndex numOptions
  volGridAndATMMap = do
  if optionIndex == numOptions
  then do
    return volGridAndATMMap
  else do
    let
      valDateT = optionParams_valDt (arrOptionParams !! optionIndex)
      valDateStr = TimeF.formatTime defaultTimeLocale "%Y%m%d" valDateT
      locATMVols = DT.unpack $ optionParams_pathATM
                     (arrOptionParams !! optionIndex)
      locVolGrid = DT.unpack $ optionParams_pathGrid
                     (arrOptionParams !! optionIndex)
      volMapID = (locVolGrid,locATMVols)

    newVolGridAndATMMap <- 
      case (DM.lookup volMapID volGridAndATMMap) of
        Nothing -> do
          uncheckedVolGrid <- readVolGrid locVolGrid 
            (DT.pack valDateStr)
          evaluate(errorEvaluator(uncheckedVolGrid))
          let parsedVolGrid = parseVolGrid uncheckedVolGrid
              volGrid = errorEvaluator(parsedVolGrid)
          atmVolsText <- readVolATM locATMVols
          evaluate(errorEvaluator(atmVolsText))
          let uncheckedParsedATMVols = parseATMVols atmVolsText
          parsedATMVolsChecked <- 
            checkErrorsATMVols uncheckedParsedATMVols
          let atmVols = errorEvaluator parsedATMVolsChecked
              
          return (DM.insert volMapID (volGrid,atmVols) 
                                      volGridAndATMMap)
        _ -> do
          return volGridAndATMMap

    populateVolMap arrOptionParams (optionIndex+1) 
      numOptions newVolGridAndATMMap   
                                        
performSimForOneOption arrOptionParams optionIndex numOptions
  arrLosses arrFX arrCTP resultsDir volMap = do
  let
    id = optionParams_id (arrOptionParams !! optionIndex)
    valDate = optionParams_valDt (arrOptionParams !! optionIndex)
    matDate = optionParams_matDt (arrOptionParams !! optionIndex)
    locATMVols = DT.unpack $ optionParams_pathATM
                   (arrOptionParams !! optionIndex)
    locVolGrid = DT.unpack $ optionParams_pathGrid
                   (arrOptionParams !! optionIndex)
    optionStrike = optionParams_K (arrOptionParams !! optionIndex)
    und = optionParams_und (arrOptionParams !! optionIndex)
    liquidityMultFactor = 1.25
    lotSize = optionParams_lotSize (arrOptionParams !! optionIndex)
    minPriceChange = optionParams_minPriceChange
      (arrOptionParams !! optionIndex)
    curr = DT.toLower $ optionParams_curr (arrOptionParams !! optionIndex)
    optionType = optionParams_type (arrOptionParams !! optionIndex)
    optionProdName = optionParams_prodName (arrOptionParams !! optionIndex)
    arrLossesUnd = filter (\x -> (varL_inst x) == und) arrLosses
    volMapID = (locVolGrid,locATMVols)
  evaluate(if arrLossesUnd == [] then error "Unable to find underlying in the losses file" else ())

  let (volGrid,scaledATMVolReturns) =
        case (DM.lookup volMapID volMap) of
          Nothing -> error "Could not find volatilities"
          (Just m) -> m

  let listFXDates = map fx_Date arrFX
      listVarLossDates = map varL_Date arrLosses
      listContracts = map cp_instr arrCTP
  
      setFXDates = DS.fromList listFXDates
      setVarLossDates = DS.fromList listVarLossDates
      
      setMissingDatesInFX =
        DS.difference setVarLossDates setFXDates
      listFXRates = map fx_scen arrFX
      fxPair = zip listFXDates listFXRates
      fxMap = DM.fromList fxPair
      doFX = if (curr == DT.pack "eur") then True
             else
               if (curr == DT.pack "gbp") then False
               else error "invalid currency"

      listContractPrices = map cp arrCTP
      cpPair = zip listContracts listContractPrices
      cpMap = DM.fromList cpPair
  
      unfx arg =
        if doFX == False
        then arg
        else                       
          let jfx = DM.lookup (varL_Date arg) fxMap
              result Nothing = error $ "Date " ++ (show (varL_Date arg)) ++ " not found in fx scenarios" 
              result (Just fx) =
                VarLosses {
                  varL_inst = varL_inst arg,
                  varL_Date = varL_Date arg,
                  varL_Loss = (varL_Loss arg)*fx
                }
          in result jfx
             
      justUnFXedLosses = map unfx arrLossesUnd

  evaluate(if setMissingDatesInFX == DS.empty then () else error "Error : there are dates in var losses which are not present in fx scenarios")

  let price = case DM.lookup und cpMap of
        Nothing -> error $ "Error : Contract " ++ (show und) ++ " not found"
        (Just p) -> p
      scenarioDatePrices =
        map (\x -> (convertDayToExcelInt $ varL_Date x,
                    price + ((varL_Loss x)/(lotSize*liquidityMultFactor))))
        justUnFXedLosses
      tm = (fromIntegral ((convertDayToExcelInt matDate) - (convertDayToExcelInt valDate)))/365.0
      callOrPutInt = if optionType == (DT.pack "c") then 1 else 0
      stirOrNotInt = if (DT.take 4 optionProdName) == (DT.pack "stir")
                     then 1
                     else 0
                          
  resultsOptionSims <- performOptionScenPricesCalc
    price optionStrike tm
    callOrPutInt stirOrNotInt
    scenarioDatePrices
    scaledATMVolReturns
    volGrid

  printToFile_COptionScenPrices resultsDir id resultsOptionSims
    (CDouble minPriceChange)

  let
    unroundedBaseOptionPrice = cOptionScenPrices_optionPrice resultsOptionSims
    baseOptionPrice = roundFnMinPriceChange (CDouble minPriceChange) $
      unroundedBaseOptionPrice
    scenDates = cIntArr_iarr $ cOptionScenPrices_dates
      resultsOptionSims
    unroundedScenOptPrices = cDblArr_darr $
      cOptionScenPrices_scenOptionPrices
      resultsOptionSims
    scenOptPrices = map (roundFnMinPriceChange
      (CDouble minPriceChange)) unroundedScenOptPrices
    scenOptLosses = map ((-baseOptionPrice)+) scenOptPrices
    datePrice = zip scenDates scenOptLosses

    dofx (a,b) = b / (CDouble fx) where
      fx = case DM.lookup (convertExcelIntToDay (fromIntegral a)) fxMap of
        Nothing -> error $ "No fx found for date " ++
          (show $ convertExcelIntToDay (fromIntegral a)) ++
          " when fxing option scenarios" 
        Just m -> m
    scenCtLosses = map (\(x,y) -> (x,y*(CDouble lotSize))) $
                   map (\(x,y) -> (x,y*(CDouble liquidityMultFactor))) datePrice
    scenOptLossesFinal = map dofx scenCtLosses
    sortedLosses = DL.sort scenOptLossesFinal
    es = ((sortedLosses !! 0) + (sortedLosses !! 1) +
      (sortedLosses !! 2) + (sortedLosses !! 3))/4

  printToFile_OptionContractInfo resultsDir id es scenDates
    scenOptLossesFinal

  let summary = SummaryResults {
    summaryResults_id = id,
    summaryResults_contractPrice = (cOptionScenPrices_contractPrice resultsOptionSims),
    summaryResults_interpolatedBaseVol = (cOptionScenPrices_interpolatedBaseVol resultsOptionSims),
    summaryResults_strike = (cOptionScenPrices_strike resultsOptionSims),
    summaryResults_tm = (cOptionScenPrices_tm resultsOptionSims),
    summaryResults_callOrPut = (cOptionScenPrices_callOrPut resultsOptionSims),
    summaryResults_stirOrNot = (cOptionScenPrices_stirOrNot resultsOptionSims),
    summaryResults_optionPrice = (cOptionScenPrices_optionPrice resultsOptionSims),
    summaryResults_lotSize = (CDouble lotSize),
    summaryResults_es = es       
  }

  return summary

printToFile_Summary resultsDir arrSummary = do
  fH <- openFile (resultsDir ++ "/summary.csv") WriteMode
  hPutStrLn fH summaryHeader
  print fH arrSummary
  hClose fH
  where
    print fH [] = do { (hClose fH); return () }
    print fH (x:xs) = do
      hPutStrLn fH $ summaryToString x
      print fH xs

printToFile_OptionContractInfo
  resultsDir id es arrDates arrLosses = do
  fH <- openFile (resultsDir ++ "/option_contract_losses_" ++
    (DT.unpack id) ++ "_.csv") WriteMode

  hPutStrLn fH $ "Expected shortfall," ++ (show es)

  hPutStrLn fH ""
  printToFile fH ["Dates"] ["Option contract losses"]
  printToFile fH arrDates arrLosses
  hClose fH
  where
    printToFile fH [] [] = return ()
    printToFile fH (x1:xs1) (x2:xs2) = do
      hPutStrLn fH $ (show x1) ++ "," ++ (show x2)
      printToFile fH xs1 xs2

printToFile_COptionScenPrices resultsDir id resultsOptionSims
  cMinPriceChange = do
  fH <- openFile (resultsDir ++ "/option_scen_" ++ (DT.unpack id)
    ++ "_.csv") WriteMode
        
  hPutStrLn fH $ "Contract price," ++
    (show $ cOptionScenPrices_contractPrice resultsOptionSims)
  hPutStrLn fH $ "Interpolated base vol," ++
    (show $ cOptionScenPrices_interpolatedBaseVol resultsOptionSims)
  hPutStrLn fH $ "Strike," ++
    (show $ cOptionScenPrices_strike resultsOptionSims)
  hPutStrLn fH $ "tm," ++
    (show $ cOptionScenPrices_tm resultsOptionSims)
  hPutStrLn fH $ "callOrPut," ++
    (show $ cOptionScenPrices_callOrPut resultsOptionSims)
  hPutStrLn fH $ "stirOrNot," ++
    (show $ cOptionScenPrices_stirOrNot resultsOptionSims)
  hPutStrLn fH $ "optionPrice (unrounded)," ++
    (show $ cOptionScenPrices_optionPrice resultsOptionSims)
  hPutStrLn fH $ "optionPrice (rounded)," ++
    (show $ (roundFnMinPriceChange cMinPriceChange) $
    cOptionScenPrices_optionPrice resultsOptionSims)

  hPutStrLn fH ""
  printToFile fH ["Dates"] ["vols"] ["scenContractPrices"] ["scenOptionPrices (unrounded)"] ["scenOptionPrices (rounded)"]
  let a1 = (cIntArr_iarr $ cOptionScenPrices_dates resultsOptionSims)
      a2 = (cDblArr_darr $ cOptionScenPrices_vols resultsOptionSims)
      a3 = (cDblArr_darr $ cOptionScenPrices_scenContractPrices resultsOptionSims)
      a4 = (cDblArr_darr $ cOptionScenPrices_scenOptionPrices resultsOptionSims)
      a5 = (map (roundFnMinPriceChange cMinPriceChange)
             (cDblArr_darr $ cOptionScenPrices_scenOptionPrices
               resultsOptionSims))
  printToFile fH a1 a2 a3 a4 a5
  hClose fH
  where
  printToFile fH [] [] [] [] [] = return ()
  printToFile fH (x1:xs1) (x2:xs2) (x3:xs3) (x4:xs4) (x5:xs5) = do
    hPutStrLn fH $ (show x1) ++ "," ++
                   (show x2) ++ "," ++
                   (show x3) ++ "," ++
                   (show x4) ++ "," ++
                   (show x5)
    printToFile fH xs1 xs2 xs3 xs4 xs5

printToFile_CTimeSeries resultsDir nameOfCurve tm dates
  volScalRets = do
  fH <- openFile (resultsDir ++ "/scaled_returns_" ++ nameOfCurve) WriteMode
  hPutStrLn fH $ "," ++ (init $ tail $ show tm)
  printRestCTimeSeries fH dates volScalRets
  where 
    printRestCTimeSeries fH [] [] = do
      hClose fH
      return ()
    printRestCTimeSeries fH (d:ds) (v:vs) = do
      hPutStrLn fH $ (show d) ++ "," ++ (init $ tail $ show v)     
      printRestCTimeSeries fH ds vs

main = do
  --(inputFileLoc:_) <- SysEnv.getArgs
  --runSim inputFileLoc
  results <- return []
  runSim "input.txt"
  return ()
