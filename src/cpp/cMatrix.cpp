#include "dataStructure.h"
#include "MyContainers.h"
#include <algorithm>
#include <CellMatrix.h>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <stdexcept>
#include <stdio.h>
#include <string.h>
#include <utility>
#include <vector>

#include <gsl/gsl_cdf.h>
#include <gsl/gsl_sf_bessel.h>
#include <gsl/gsl_statistics_double.h>

#define ERROR -1

typedef uint32_t ui;

extern "C" {

  int cDblArr_allocate(cDblArr* p, ui size);
  int cDblArr_allocateByRef(cDblArr* &p, ui size);
  int cIntArr_allocate(cIntArr* p, ui size);
  int cIntArr_allocateByRef(cIntArr* &p, ui size);
  int cMatrix_allocate(cMatrix* p, ui sizex, ui sizey);
  int cMatrix_allocateByRef(cMatrix* &p, ui sizex, ui sizey);
  int cTimeSeries_allocate(cTimeSeries* p);
  int cOptionScenPrices_allocate(cOptionScenPrices* p);
  int allocate_from_matrix(cMatrix* &p, 
     std::vector<std::vector<double > > m);
  int allocate_from_vectorInt(cIntArr* &p,
     std::vector<int> v);
  int allocate_from_vectorDbl(cDblArr* &p,
     std::vector<double> v);

  int copyDblArr(cDblArr*& to, cDblArr* from);

  void cIntArr_free(cIntArr *p);
  void cMatrix_free(cMatrix *p);
  void cTimeSeries_free(cTimeSeries *p);
  void cOptionScenPrices_free(cOptionScenPrices *p);
  void cDblArr_free(cDblArr *p);

}

double getM(const xlw::dtdbpairvecSingle& arg)
{
    int sizeArr = arg.size();
    double mean = 0.;
    for(int i=0; i<sizeArr; ++i)
    {
        mean += arg[i].second;
    }
    mean /= double(sizeArr);
    return mean;
}

double getSD(const xlw::dtdbpairvecSingle& arg)
{
    int sizeArr = arg.size();
    double mean = getM(arg);
    double secondMom = 0.;
    for(int i=0; i<sizeArr; ++i)
    {
        secondMom += pow(arg[i].second - mean,2.0);
    }
    secondMom /= double(sizeArr-1);
    secondMom = pow(secondMom,0.5);
    return secondMom;
}

class constructSR
{
public:
    std::vector<xlw::dtdbpairvec> cls_r;
    std::vector<xlw::dtdbpairvecSingle> cls_rP;
    std::vector<double> cls_rmh;
    xlw::dtdbpairvec cls_e;
    xlw::dtdbpairvecSingle cls_eP;
    std::vector<xlw::dtdbpairvec> cls_sr;
    std::vector<xlw::dtdbpairvecSingle> cls_srp;
    xlw::CellMatrix cls_m;
    xlw::dtdbpairvec cls_tsmd;
    xlw::dtdbpairvecSingle cls_tsmdp;

    bool cls_e1d;
    bool cls_sm;
    bool cls_1e;
    bool cls_a;
    bool cls_mr;
    bool cls_useLegacy;

    int getRowR(int date, const xlw::dtdbpairvecSingle& arg) const;
    int getRowR(int date, const xlw::dtdbpairvec& arg) const;
    int cls_hp;
    int cls_holdingPeriodUse;
    int cls_maxIndex_rets;
    int cls_numScen;
    int cls_seedN;
    int cls_scal;
    int cls_lastEFunc;
    int cls_numVolsLT;
    int cls_numVolsP;
    double cls_volP;
    bool cls_a1dsr;

    xlw::MyArray cls_la;
    double cls_per;

    std::string cls_method;

    enum arrayIndices_Rows {index_totalNumber=0,
                            index_upperBreaches=1,
                            index_lowerBreaches=2,
                            index_totalBreaches=3,
                            index_l_00=4,
                            index_l_01=5,
                            index_l_10=6,
                            index_l_11=7,
                            index_pi=8,
                            index_pi_0=9,
                            index_pi_1=10,
                            index_numberInExpectedShortFall=11,
                            index_passed_kupiec=12,
                            index_passed_christoffersen=13,
                            index_fields=14,
                            startRow = 15
                           };

    enum arrayIndices_Columns {index_date=0,
                               index_P_and_L=1,
                               index_lower_es=2,
                               index_upper_es=3,
                               index_lower_var=4,
                               index_upper_var=5,
                               num_cols=6
                              };

    enum arrayIndices_Columns_scen {scen_index_date=0,
                                    scen_index_change_in_value=1,
                                    scen_index_label_isantithetic=2,
                                    scen_num_cols = 3
                                   };

public :
    constructSR(int i_hp, int i_numScen,
                           bool i_useOneSetofEWMA, const xlw::dtdbpairvec& i_tsMarketData, bool i_generateEWMAFrom1Day,
                           bool i_scaledMargins, int i_seedVolNumber,xlw::MyArray i_lambda, std::string i_method,
                           bool i_useAntithetic, double i_percentile,int i_returnScalingMethod, bool i_useMaxReturns,
                           bool i_useLegacy, int i_lastestEWMAScalingFunctionalForm, int i_numVolsLT,
                           int i_numVolsPercentile, double i_volPercentile, bool i_aggregate1DScaledReturns);
    void setR(const xlw::dtdbpairvec& arg, int hp,
                    std::string method,std::vector<xlw::dtdbpairvec>& returns) const;
    void setRP(const xlw::dtdbpairvecSingle& arg, int hp,
                             std::string method,std::vector<xlw::dtdbpairvecSingle>& returns) const;
    void setE(const xlw::dtdbpairvec& retTS,
                     int dt, int seedVolN, xlw::MyArray lambdaArr,
                     bool useLegacy, int numScen, xlw::dtdbpairvec& ewma) const;
    void setEP(const xlw::dtdbpairvecSingle& retTS,
                              int dt, int seedVolN, xlw::MyArray lambdaArr,
                              bool useLegacy, int numScen, xlw::dtdbpairvecSingle& ewma) const;
    void setSR(const xlw::dtdbpairvec& retTS,
                          const xlw::dtdbpairvec& ewmaTS, int retScal, int dateOfSimulation,
                          bool ewmaFrom1Day, int hp, bool useLegacy, int numScen,
                          std::vector<xlw::dtdbpairvec>& sR) const;
    void setSRP(const xlw::dtdbpairvecSingle& retTS,
                                   const xlw::dtdbpairvecSingle& ewmaTS, int retScal, int dateOfSimulation,
                                   bool ewmaFrom1Day,int hp, bool useLegacy, int numScen,
                                   std::vector<xlw::dtdbpairvecSingle>& sR) const;
    void setM(const xlw::dtdbpairvec& tsMarketData, int hp,
                    int seedVolN, xlw::MyArray lambdaArr, std::string method,
                    bool useAntithetic, double percentile, int retScal,
                    bool ewmaFrom1Day, bool useMaxReturns, bool useLegacy,
                    bool useOneSetofEWMA, bool scaledMargins, const xlw::dtdbpairvecSingle& tsMarketDataPortfolio,
                    bool scaleRiskFactorsSeparately);
    xlw::dtdbpairvec getR(int index) const;
    std::vector<xlw::dtdbpairvec>& getRArrRef(void);
    xlw::dtdbpairvec getE() const;
    xlw::dtdbpairvec& getERef();
    xlw::dtdbpairvec getSR(int index) const;
    std::vector<xlw::dtdbpairvec>& getSRArrRef(void);
    xlw::CellMatrix getMargins() const;
    xlw::dtdbpairvecSingle getMarketDataPortfolio() const;
    static double log_K(int num_breaches, int num_total, double percentile);
    static double log_C(int l_00, int l_01, int l_10, int l_11);
    static bool hasPassed_K(int num_breaches, int num_total, double varPercentile, double kupiecPercentile);
    static bool hasPassed_C(int l_00, int l_01, int l_10, int l_11, double percentile);
    int getIndexLM(const std::vector<double>& i_returnsMH);
    int getIndexLM(const std::vector<xlw::dtdbpairvecSingle>& i_scaledReturns,
                   int scen, std::vector<double>& i_returnsMH);
    int getIndexLMScaled(const std::vector<xlw::dtdbpairvecSingle>& i_scaledReturns,
                         int scen, std::vector<double>& i_returnsMH);
};

int  constructSR::getRowR
(int date, const xlw::dtdbpairvecSingle& arg) const
{
    int sizeArray, i;
    sizeArray = arg.size();

    for(i=0; i<sizeArray; ++i)
    {
        if(arg[i].first == date) {
            break;
        }
    }

    return i;
}

int  constructSR::getRowR
(int date, const xlw::dtdbpairvec& arg) const
{
    int sizeArray, i;
    sizeArray = arg.size();

    for(i=0; i<sizeArray; ++i)
    {
        if(arg[i].first == date) {
            break;
        }
    }

    return i;
}

void constructSR::setR
(const xlw::dtdbpairvec& arg, int hp,
 std::string method, std::vector<xlw::dtdbpairvec>& r) const
{
    if(hp > int(r.size())) {
        throw std::runtime_error("error");
    }

    int j;
    int maxIndex = arg.size() - hp;
    xlw::dtdbpairvec results(maxIndex);
    int numFactors = arg[0].second.size();
    double ret;

    if(method == "relative") {
        for(int i=0; i<maxIndex; ++i) {
            results[i].first = arg[i].first;
            for(j = 0; j<numFactors; ++j) {
                ret = (arg[i].second[j]/arg[i+hp].second[j]) - 1.;
                results[i].second.push_back(ret);
            }
        }
    } else if(method == "absolute") {
        for(int i=0; i<maxIndex; ++i) {
            results[i].first = arg[i].first;
            for(j = 0; j<numFactors; ++j) {
                ret =  arg[i].second[j] - arg[i+hp].second[j];
                results[i].second.push_back(ret);
            }
        }
    } else {
        throw std::runtime_error("error");
    }

    r[hp-1] = results;
}

void constructSR::setRP
(const xlw::dtdbpairvecSingle& arg, int hp,
 std::string method, std::vector<xlw::dtdbpairvecSingle>& r) const
{
    if(hp > int(r.size())) {
        throw std::runtime_error("error");
    }

    int maxIndex = arg.size() - hp;
    xlw::dtdbpairvecSingle results(maxIndex);
    double ret;

    if(method == "relative") {
        for(int i=0; i<maxIndex; ++i) {
            results[i].first = arg[i].first;
            ret = (arg[i].second/arg[i+hp].second) - 1.;
            results[i].second=ret;
        }
    } else if(method == "absolute") {
        for(int i=0; i<maxIndex; ++i) {
            results[i].first = arg[i].first;
            ret =  arg[i].second - arg[i+hp].second;
            results[i].second =ret;
        }
    } else {
        throw std::runtime_error("error");
    }

    r[hp-1] = results;
}

void constructSR::setEP
(const xlw::dtdbpairvecSingle& retTS, int dt,
 int seedVolN, xlw::MyArray lambdaArr, bool useLegacy,
 int numScen, xlw::dtdbpairvecSingle& ewma) const
{
    xlw::dtdbpairvecSingle stdVec;
    int sizeRets = retTS.size();
    int dateIndex;
    double stdDevSeed, lambda;
    bool useSingleLambda;

    if(lambdaArr.size() == 1) {
        lambda = lambdaArr[0];
        useSingleLambda = true;
    } else {
        useSingleLambda = false;
        if(lambdaArr.size() != retTS.size()) {
            throw std::runtime_error("error");
        }
    }

    dateIndex = getRowR(dt, retTS);

    if((dateIndex + (numScen-1) + seedVolN)>=sizeRets)
    {
        throw std::runtime_error("error");
    }
    for(int i=0; i<seedVolN; ++i)
    {
        stdVec.push_back(retTS[dateIndex+ numScen + i]);
    }

    stdDevSeed = getSD(stdVec);

    ewma[numScen].first = retTS[dateIndex + numScen].first;
    ewma[numScen].second = stdDevSeed;

    if(useSingleLambda == true) {
        for(int i = numScen; i>0; --i)
        {
            ewma[i-1].first = retTS[dateIndex + (i-1)].first;

            if(useLegacy == true) {
                if(i==numScen) {
                    ewma[i-1].second = lambda* pow(stdDevSeed,2.0) +
                                       (1.-lambda)*pow(retTS[dateIndex + i].
                                                       second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                } else {
                    ewma[i-1].second = lambda* pow(ewma[i].second,2.0) +
                                       (1.-lambda)*pow(retTS[dateIndex + i].
                                                       second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                }
            } else {
                if(i==numScen) {
                    ewma[i-1].second = lambda* pow(stdDevSeed,2.0) +
                                       (1.-lambda)*pow(retTS[dateIndex + (i-1)].
                                                       second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                } else {
                    ewma[i-1].second = lambda* pow(ewma[i].second,2.0) +
                                       (1.-lambda)*pow(retTS[dateIndex + (i-1)].
                                                       second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                }
            }
        }
    } else {
        for(int i = numScen; i>0; --i)
        {
            ewma[i-1].first = retTS[dateIndex + (i-1)].first;

            if(useLegacy == true) {
                if(i==numScen) {
                    ewma[i-1].second = lambdaArr[i-1]* pow(stdDevSeed,2.0) +
                                       (1.-lambdaArr[i-1])*pow(retTS[dateIndex + i].
                                               second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                } else {
                    ewma[i-1].second = lambdaArr[i-1]* pow(ewma[i].second,2.0) +
                                       (1.-lambdaArr[i-1])*pow(retTS[dateIndex + i].
                                               second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                }
            } else {
                if(i==numScen) {
                    ewma[i-1].second = lambdaArr[i-1]* pow(stdDevSeed,2.0) +
                                       (1.-lambdaArr[i-1])*pow(retTS[dateIndex + (i-1)].
                                               second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                } else {
                    ewma[i-1].second = lambdaArr[i-1]* pow(ewma[i].second,2.0) +
                                       (1.-lambdaArr[i-1])*pow(retTS[dateIndex + (i-1)].
                                               second,2.0);

                    ewma[i-1].second = pow(ewma[i-1].second,0.5);
                }
            }
        }
    }
}


void constructSR::setE
(const xlw::dtdbpairvec& retTS, int dt,
 int seedVolN, xlw::MyArray lambdaArr, bool useLegacy,
 int numScen, xlw::dtdbpairvec& ewma) const
{
    xlw::dtdbpairvec stdVec;
    int sizeRets = retTS.size();
    int numFactors = retTS[0].second.size();
    int dateIndex;
    double stdDevSeed, lambda;
    bool useSingleLambda;

    if(lambdaArr.size() == 1) {
        lambda = lambdaArr[0];
        useSingleLambda = true;
    } else {
        useSingleLambda = false;
        if(lambdaArr.size() != retTS.size()) {
            throw std::runtime_error("error");
        }
    }

    dateIndex = getRowR(dt, retTS);

    if((dateIndex + (numScen-1) + seedVolN)>=sizeRets)
    {
        throw std::runtime_error("error");
    }
    for(int i=0; i<seedVolN; ++i)
    {
        stdVec.push_back(retTS[dateIndex+ numScen + i]);
    }
    for(int factor = 0; factor < numFactors; ++factor) {
        stdDevSeed = 0.;
        for(int j=0; j<stdVec.size(); j++) {
            stdDevSeed += pow(stdVec[j].second[factor],2.0);
        }
        stdDevSeed /= double(stdVec.size());
        stdDevSeed = pow(stdDevSeed,0.5);



        ewma[numScen].first = retTS[dateIndex + numScen].first;
        ewma[numScen].second[factor] = stdDevSeed;

        if(useSingleLambda == true) {
            for(int i = numScen; i>0; --i)
            {
                ewma[i-1].first = retTS[dateIndex + (i-1)].first;

                if(useLegacy == true) {
                    if(i==numScen) {
                        ewma[i-1].second[factor] = lambda* pow(stdDevSeed,2.0)
                                                   + (1.-lambda)*pow(retTS[dateIndex + (i-1)].
                                                           second[factor],2.0);

                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    } else {
                        ewma[i-1].second[factor] = lambda* pow(ewma[i].second[factor],2.0)
                                                   + (1.-lambda)*pow(retTS[dateIndex + (i-1)].
                                                           second[factor],2.0);

                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    }
                } else {
                    if(i==numScen) {
                        ewma[i-1].second[factor] = lambda* pow(stdDevSeed,2.0) +
                                                   (1.-lambda)*pow(retTS[dateIndex + (i-1)].
                                                           second[factor],2.0);

                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    } else {
                        ewma[i-1].second[factor] = lambda* pow(ewma[i].second[factor],
                                                               2.0) + (1.-lambda)*pow(retTS[dateIndex + (i-1)].
                                                                       second[factor],2.0);

                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    }
                }
            }
        } else {
            for(int i = numScen; i>0; --i)
            {
                ewma[i-1].first = retTS[dateIndex + (i-1)].first;

                if(useLegacy == true) {
                    if(i==numScen) {
                        ewma[i-1].second[factor] = lambdaArr[i-1]* pow(stdDevSeed,2.0)
                                                   + (1.-lambdaArr[i-1])*pow(retTS[dateIndex + i].
                                                           second[factor],2.0);
                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    } else {
                        ewma[i-1].second[factor] = lambdaArr[i-1]* pow(ewma[i].
                                                   second[factor],2.0) + (1.-lambdaArr[i-1])*
                                                   pow(retTS[dateIndex + i].second[factor],2.0);

                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    }
                } else {
                    if(i==numScen) {
                        ewma[i-1].second[factor] = lambdaArr[i-1]* pow(stdDevSeed,2.0)
                                                   + (1.-lambdaArr[i-1])*pow(retTS[dateIndex +
                                                           (i-1)].second[factor],2.0);

                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    } else {
                        ewma[i-1].second[factor] = lambdaArr[i-1]* pow(ewma[i].
                                                   second[factor],2.0) + (1.-lambdaArr[i-1])*
                                                   pow(retTS[dateIndex + (i-1)].second[factor],2.0);

                        ewma[i-1].second[factor] = pow(ewma[i-1].second[factor],0.5);
                    }
                }
            }
        }
    }
}


std::vector<xlw::dtdbpairvec>& constructSR::getRArrRef(void)
{
    return cls_r;
}

xlw::dtdbpairvec constructSR::getE() const
{
    return cls_e;
}

void constructSR::setSR
(const xlw::dtdbpairvec& retTS,
 const xlw::dtdbpairvec& ewmaTS, int retScal,
 int dateOfSimulation, bool ewmaFrom1Day,int hp,
 bool useLegacy, int numScen, std::vector<xlw::dtdbpairvec>& sR) const
{
    int offset_rets, i, offset_ewma, numFactors, factor, j, numAggRets;
    double todaysEWMA, historicalEWMA, scalingFactor;

    numFactors = retTS[0].second.size();

    offset_rets = getRowR(dateOfSimulation, retTS);

    if(ewmaTS[0].first != retTS[0].first) {
        if(ewmaTS.size() != numScen + 1) {
            throw std::runtime_error("error");
        }
        if(ewmaTS[0].first != dateOfSimulation) {
            throw std::runtime_error("error");
        }
        offset_ewma = 0;
    } else {
        offset_ewma = offset_rets;
    }

    xlw::dtdbpairvec retArr(numScen);

    for(i=0; i<numScen; ++i) {
        retArr[i].second = std::vector<double>(numFactors);
    }

    for(factor = 0; factor < numFactors; ++factor) {

        if(cls_lastEFunc == 0) {
            todaysEWMA = ewmaTS[0+offset_ewma].second[factor];
        } else if(cls_lastEFunc == 1) {
            todaysEWMA = 0.;

            if((cls_numVolsLT+offset_ewma) == (int)ewmaTS.size())
            {
                throw std::runtime_error("error");
            }

            for(i=0; i<cls_numVolsLT; ++i) {
                todaysEWMA += ewmaTS[i+offset_ewma].second[factor];
            }
            todaysEWMA /= double(cls_numVolsLT);
        } else if(cls_lastEFunc == 2) {
            int length = cls_numVolsP;

            if((cls_numVolsP+offset_ewma) == (int)ewmaTS.size())
            {
                throw std::runtime_error("error");
            }

            xlw::CellMatrix volMat(length,1);
            for(i=0; i<length; ++i) {
                volMat(i,0) = ewmaTS[i+offset_ewma].second[factor];
            }
            volMat.sort(0);

            double *p = 0;
            volMat.GSL_Array1D_Allocate(p);

            if(p == 0) {
                throw std::runtime_error("error");
            } else {
                for(i=0; i<length; ++i) {
                    p[i] = volMat(i,0).NumericValue();
                }
                todaysEWMA = gsl_stats_quantile_from_sorted_data(p,1,length,cls_volP);
                delete p;
            }
        } else {
            throw std::runtime_error("error");
        }

        if(useLegacy == true) {
            if(retScal == 1) {
                for(i=0; i<numScen; ++i) {
                    historicalEWMA = ewmaTS[i+offset_ewma].second[factor];
                    scalingFactor = (historicalEWMA+todaysEWMA)/(2.*historicalEWMA);
                    retArr[i].first = ewmaTS[i+offset_ewma].first;
                    retArr[i].second[factor] = retTS[i+offset_rets].
                                               second[factor] * scalingFactor;
                }
            } else if(retScal == 2) {
                for(i=0; i<numScen; ++i) {
                    historicalEWMA = ewmaTS[i+offset_ewma].second[factor];
                    scalingFactor = (todaysEWMA/historicalEWMA);
                    retArr[i].first = ewmaTS[i+offset_ewma].first;
                    retArr[i].second[factor] = retTS[i+offset_rets].
                                               second[factor] * scalingFactor;
                }
            } else {
                throw std::runtime_error("error");
            }
        } else {
            if(retScal == 1) {
                for(i=0; i<numScen; ++i) {
                    historicalEWMA = ewmaTS[i+offset_ewma+1].second[factor];
                    scalingFactor = (historicalEWMA+todaysEWMA)/(2.*historicalEWMA);
                    retArr[i].first = ewmaTS[i+offset_ewma].first;
                    retArr[i].second[factor] = retTS[i+offset_rets].
                                               second[factor] * scalingFactor;
                }
            } else if(retScal == 2) {
                for(i=0; i<numScen; ++i) {
                    historicalEWMA = ewmaTS[i+offset_ewma+1].second[factor];
                    scalingFactor = (todaysEWMA/historicalEWMA);
                    retArr[i].first = ewmaTS[i+offset_ewma].first;
                    retArr[i].second[factor] = retTS[i+offset_rets].
                                               second[factor] * scalingFactor;
                }
            } else {
                throw std::runtime_error("error");
            }
        }
    }

    sR[hp-1] = retArr;
}


constructSR::constructSR
(int i_hp, int i_numScen, bool i_useOneSetofEWMA,
 const xlw::dtdbpairvec& i_tsMarketData, bool i_generateEWMAFrom1Day,
 bool i_scaledMargins, int i_seedVolNumber,xlw::MyArray i_lambda,
 std::string i_method, bool i_useAntithetic, double i_percentile,
 int i_returnScalingMethod, bool i_useMaxReturns, bool i_useLegacy,
 int i_lastestEWMAScalingFunctionalForm, int i_numVolsLT,
 int i_numVolsPercentile, double i_volPercentile, bool i_aggregate1DScaledReturns):
    cls_hp(i_hp),cls_r(i_hp),
    cls_rP(i_hp),cls_sr(i_hp),
    cls_srp(i_hp),cls_rmh(i_hp),
    cls_numScen(i_numScen),cls_1e(i_useOneSetofEWMA),
    cls_tsmd(i_tsMarketData),cls_e1d(i_generateEWMAFrom1Day),
    cls_sm(i_scaledMargins),cls_seedN(i_seedVolNumber),
    cls_la(i_lambda),cls_method(i_method),cls_a(i_useAntithetic),
    cls_per(i_percentile),cls_scal(i_returnScalingMethod),
    cls_mr(i_useMaxReturns),cls_useLegacy(i_useLegacy),
    cls_tsmdp(i_tsMarketData.size()),
    cls_lastEFunc(i_lastestEWMAScalingFunctionalForm),
    cls_numVolsLT(i_numVolsLT),
    cls_numVolsP(i_numVolsPercentile),
    cls_volP(i_volPercentile),
    cls_a1dsr(i_aggregate1DScaledReturns)
{
    if(cls_a1dsr == true) {
        cls_numScen = cls_numScen + (cls_hp-1);

        if(cls_sm == true) {
            throw std::runtime_error("error");
        }

        if(cls_mr == true) {
            throw std::runtime_error("error");
        }

        if(cls_method == "relative") {
            throw std::runtime_error("error");
        }

        if(cls_a == true) {
            throw std::runtime_error("error");
        }

        if(cls_1e == false) {
            throw std::runtime_error("error");
        }
    }

    int numData = i_tsMarketData.size();
    int numFactors = cls_tsmd[0].second.size();
    double portfolioVal;

    for(int i=0; i<numData; ++i) {
        cls_tsmdp[i].first = cls_tsmd[i].first;
        portfolioVal = 0.;
        for(int j=0; j<numFactors; ++j) {
            portfolioVal += cls_tsmd[i].second[j];
        }
        cls_tsmdp[i].second = portfolioVal;
    }

    if(cls_mr == true) {
        if(cls_e1d == false) {
            throw std::runtime_error("error");
        }
        if(cls_1e == true) {
            throw std::runtime_error("error");
        }
    }

    if(cls_sm == true && cls_e1d == false) {
        throw std::runtime_error("error");
    }

    if(cls_mr == false) {
        if(cls_a1dsr == false) {
            if(cls_sm == false) {
                setR(cls_tsmd, cls_hp,cls_method,cls_r);

                setRP(cls_tsmdp, cls_hp,
                                    cls_method,cls_rP);
            }

            if(cls_e1d == true || cls_sm == true) {
                setR(cls_tsmd,1,cls_method,cls_r);

                setRP(cls_tsmdp,1,cls_method,
                                    cls_rP);
            }
        } else {
            setR(cls_tsmd,1,cls_method,cls_r);

            setRP(cls_tsmdp,1,cls_method,
                                cls_rP);
        }
    } else {
        for(int i=1; i<=cls_hp; ++i) {
            setR(cls_tsmd,i,cls_method,cls_r);

            setRP(cls_tsmdp,i,cls_method,
                                cls_rP);
        }
    }

    if(cls_1e == true) {
        if(cls_e1d == true || cls_sm == true) {
            cls_holdingPeriodUse = 1;
        } else {
            cls_holdingPeriodUse = cls_hp;
        }
        cls_maxIndex_rets = (cls_r[cls_holdingPeriodUse-1].size()-1)
                            - cls_seedN;

        cls_e = xlw::dtdbpairvec((cls_maxIndex_rets+1)+1);
        cls_eP = xlw::dtdbpairvecSingle((cls_maxIndex_rets+1)+1);

        for(int i=0; i<((cls_maxIndex_rets+1)+1); ++i) {
            cls_e[i].second = std::vector<double>(numFactors);
        }

        setE(cls_r[cls_holdingPeriodUse-1],
                    cls_r[cls_holdingPeriodUse-1][0].first,
                    cls_seedN, cls_la, cls_useLegacy,
                    cls_maxIndex_rets+1, cls_e);

        setEP(cls_rP[cls_holdingPeriodUse-1],
                             cls_rP[cls_holdingPeriodUse-1][0].first,
                             cls_seedN, cls_la, cls_useLegacy,
                             cls_maxIndex_rets+1, cls_eP);
    } else {
        cls_holdingPeriodUse = cls_hp;
        cls_e = xlw::dtdbpairvec(i_numScen+1);
        cls_eP = xlw::dtdbpairvecSingle(i_numScen+1);

        for(int i=0; i<(i_numScen+1); ++i) {
            cls_e[i].second = std::vector<double>(numFactors);
        }

        setE(cls_r[cls_holdingPeriodUse-1],
                    cls_r[cls_holdingPeriodUse-1][0].first,
                    cls_seedN,
                    cls_la,
                    cls_useLegacy,
                    i_numScen,
                    cls_e);
    }

    if(cls_lastEFunc != 0 &&
            cls_lastEFunc != 1 &&
            cls_lastEFunc != 2
      ) {
        throw std::runtime_error("error");
    }

}

int cMatrix_allocate(cMatrix* p, ui sizex, ui sizey)
{
    p->sizex = sizex;
    p->sizey = sizey;

    double** table = NULL;
    table = (double**) malloc( sizex * sizeof (double*));

    if(table == NULL) {
        return ERROR;
    }

    for(ui i=0; i<sizex; i++) {
        table[i] = NULL;
        table[i] = (double*) malloc(sizey * sizeof(double));
        if(table[i] == NULL) {
            for(ui j=0; j<=i; j++) {
                free(table[i]);
            }
            return ERROR;
        }
    }

    p->table = table;
    return 0;
}

int cMatrix_allocateByRef(cMatrix* &p, ui sizex, ui sizey)
{
    p->sizex = sizex;
    p->sizey = sizey;

    double** table = NULL;
    table = (double**) malloc( sizex * sizeof (double*));

    if(table == NULL) {
        return ERROR;
    }

    for(ui i=0; i<sizex; i++) {
        table[i] = NULL;
        table[i] = (double*) malloc(sizey * sizeof(double));
        if(table[i] == NULL) {
            for(ui j=0; j<=i; j++) {
                free(table[i]);
            }
            return ERROR;
        }
    }

    p->table = table;
    return 0;
}

int cIntArr_allocate(cIntArr* p, ui size)
{
    p->size = size;

    int* iarr = NULL;
    iarr = (int*) malloc( size * sizeof (int));

    if(iarr == NULL) {
        return ERROR;
    }

    p->iarr = iarr;
    return 0;
}

int cIntArr_allocateByRef(cIntArr* &p, ui size)
{
    p->size = size;

    int* iarr = NULL;
    iarr = (int*) malloc( size * sizeof (int));

    if(iarr == NULL) {
        return ERROR;
    }

    p->iarr = iarr;
    return 0;
}

int cDblArr_allocate(cDblArr* p, ui size)
{
    p->size = size;

    double* darr = NULL;
    darr = (double*) malloc( size * sizeof (double));

    if(darr == NULL) {
        return ERROR;
    }

    p->darr = darr;
    return 0;
}

int cDblArr_allocateByRef(cDblArr* &p, ui size)
{
    p->size = size;

    double* darr = NULL;
    darr = (double*) malloc( size * sizeof (double));

    if(darr == NULL) {
        return ERROR;
    }

    p->darr = darr;
    return 0;
}

int cTimeSeries_allocate(cTimeSeries* p)
{
    p->tm = NULL;
    p->dates = NULL;
    p->vols = NULL;
    return 0;
}

int cOptionScenPrices_allocate(cOptionScenPrices* p) {
    p->dates = NULL;
    p->vols = NULL;
    p->scenContractPrices = NULL;
    p->scenOptionPrices = NULL;
    return 0;
}

void cMatrix_free(cMatrix *p) {
    if(p == NULL) {
        return;
    }

    ui sizex = p->sizex;
    double** table = p->table;

    if(table == NULL) {
        return;
    }

    for(ui i=0; i<sizex; i++) {
        free(table[i]);
    }

    free(table);
    p->table = 0;
}

void cIntArr_free(cIntArr *p) {
    if(p == NULL) {
        return;
    }

    ui size = p->size;
    int* iarr = p->iarr;

    if(iarr == NULL) {
        return;
    }

    free(iarr);
    p->iarr = 0;
}

void cTimeSeries_free(cTimeSeries *p) {
    if(p == NULL) {
        return;
    }
    cDblArr* tm = p->tm;
    cIntArr* dates = p->dates;
    cMatrix* vols = p->vols;

    if(tm != NULL) {
        cDblArr_free(tm);
        tm = NULL;
    }

    if(dates != NULL) {
        cIntArr_free(dates);
        dates = NULL;
    }

    if(vols != NULL) {
        cMatrix_free(vols);
        vols = NULL;
    }
}

void cOptionScenPrices_free(cOptionScenPrices *p) {
    if(p == NULL) {
        return;
    }
    cIntArr *dates = p->dates;
    cDblArr *vols = p->vols;
    cDblArr *scenContractPrices = p->scenContractPrices;
    cDblArr *scenOptionPrices = p->scenOptionPrices;

    if(dates != NULL) {
        cIntArr_free(dates);
        dates = NULL;
    }
    if(vols != NULL) {
        cDblArr_free(vols);
        vols = NULL;
    }
    if(scenContractPrices != NULL) {
        cDblArr_free(scenContractPrices);
        scenContractPrices = NULL;
    }
    if(scenOptionPrices != NULL) {
        cDblArr_free(scenOptionPrices);
        scenOptionPrices = NULL;
    }
}

void cDblArr_free(cDblArr *p) {
    if(p == NULL) {
        return;
    }

    ui size = p->size;
    double* darr = p->darr;

    if(darr == NULL) {
        return;
    }

    free(darr);
    p->darr = 0;
}

int allocate_from_matrix(cMatrix* &p,
                         std::vector<std::vector<double > > m)
{
    if(p != NULL) {
        return ERROR;
    }
    p = (cMatrix*) malloc(sizeof(cMatrix));
    int rows = m.size();
    if(rows == 0) return ERROR;
    int cols = m[0].size();

    int ret = cMatrix_allocateByRef(p, rows, cols);

    if(ret != ERROR) {
        for(int r=0; r<rows; r++) {
            for(int c=0; c<cols; c++) {
                p->table[r][c] = m[r][c];
            }
        }
    }

    return ret;
}

int allocate_from_vectorInt(cIntArr* &p,
                            std::vector<int> v)
{
    if(p != NULL) {
        return ERROR;
    }
    p = (cIntArr*) malloc(sizeof(cIntArr));

    int rows = v.size();
    if(rows == 0) return ERROR;

    int ret = cIntArr_allocateByRef(p,rows);

    if(ret != ERROR) {
        for(int r=0; r<rows; r++) {
            p->iarr[r] = v[r];
        }
    }

    return ret;
}

int allocate_from_vectorDbl(cDblArr* &p,
                            std::vector<double> v)
{
    if(p != NULL) {
        return ERROR;
    }
    p = (cDblArr*) malloc(sizeof(cDblArr));

    int rows = v.size();
    if(rows == 0) return ERROR;

    int ret = cDblArr_allocateByRef(p,rows);

    if(ret != ERROR) {
        for(int r=0; r<rows; r++) {
            p->darr[r] = v[r];
        }
    }

    return ret;
}

int copyDblArr(cDblArr*& to, cDblArr* from) {
    if(from == NULL || to != NULL) {
        return ERROR;
    }
    int size = from -> size;

    to = (cDblArr*) malloc(sizeof(cDblArr));
    int success = cDblArr_allocateByRef(to,size);

    if(success != ERROR) {
        for(int i=0; i<size; i++) {
            to->darr[i] = from->darr[i];
        }
    }

    return success;
    return 0;
}


template<typename T>
std::vector<std::vector<T> > transpose
(std::vector<std::vector<T> > arg) {

    int rows = arg.size();
    int cols = arg[0].size();

    std::vector<std::vector<T> > ret =
        std::vector<std::vector<T> >(cols);

    for(int r=0; r<ret.size(); r++) {
        ret[r] = std::vector<T>(rows);
        for(int c=0; c<ret[r].size(); c++) {
            ret[r][c] = arg[c][r];
        }
    }

    return ret;

}


template<typename T>
std::vector<T> copy1DArrayToVector(int size, T* arr) {
    std::vector<T> result = std::vector<T>(size);
    for(int i=0; i<size; i++) result[i] = arr[i];
    return result;
}

template<typename T>
std::vector<std::vector<T> >
copy2DArrayToVector(int rows, int cols, T** arr) {

    std::vector<std::vector<T> > ret =
        std::vector<std::vector<T> >(rows);

    for(int i=0; i<rows; i++)
        ret[i] = copy1DArrayToVector(cols,arr[i]);

    return ret;
}

static inline uint64_t doubleToLongBits(double x) {
    uint64_t bits;
    memcpy(&bits,&x, sizeof bits);
    return bits;
}

int binarySearch0(std::vector<double> a, int fromIndex,
                  int toIndex,double key) {

    int low = fromIndex;
    int high = toIndex - 1;

    while (low <= high) {
        int mid = (low + high) >> 1;
        double midVal = a[mid];

        if (midVal < key)
            low = mid + 1;
        else if (midVal > key)
            high = mid - 1;
        else {
            long midBits = doubleToLongBits(midVal);
            long keyBits = doubleToLongBits(key);
            if (midBits == keyBits)
                return mid;
            else if (midBits < keyBits)
                low = mid + 1;
            else
                high = mid - 1;
        }
    }
    return -(low + 1);
}

int binarySearch(std::vector<double> a, double key) {
    return binarySearch0(a, 0, a.size(), key);
}

std::vector<double> interpLinear
(std::vector<double> x, std::vector<double> y,
 std::vector<double> xi, bool linearExtrapolation) {

    if (x.size() != y.size()) {
        throw std::runtime_error("error");
    }
    if (x.size() == 1) {
        throw std::runtime_error("error");
    }
    double* dx = new double[x.size() - 1];
    double* dy = new double[x.size() - 1];
    double* slope = new double[x.size() - 1];
    double* intercept = new double[x.size() - 1];

    for (int i = 0; i < x.size() - 1; i++) {
        dx[i] = x[i + 1] - x[i];
        if (dx[i] == 0) {
            throw std::runtime_error("error");
        }
        if (dx[i] < 0) {
            throw std::runtime_error("error");
        }
        dy[i] = y[i + 1] - y[i];
        slope[i] = dy[i] / dx[i];
        intercept[i] = y[i] - x[i] * slope[i];
    }

    std::vector<double> yi = std::vector<double>(xi.size());
    for (int i = 0; i < xi.size(); i++) {
        if (xi[i] > x[x.size() - 1]) {
            if (linearExtrapolation == true) {
                double deltay = y[x.size() - 1] - y[x.size() - 2];
                double deltax = x[x.size() - 1] - x[x.size() - 2];
                yi[i] = y[x.size() - 1] +
                        (xi[i] - x[x.size() - 1]) * (deltay / deltax);
                if (yi[i] < 0.) {
                    yi[i] = 0.;
                }
            } else {
                yi[i] = y[x.size() - 1];
            }
        } else if (xi[i] < x[0]) {
            if (linearExtrapolation == true) {
                double deltay = y[1] - y[0];
                double deltax = x[1] - x[0];
                yi[i] = y[0] + (xi[i] - x[0]) * (deltay / deltax);
                if (yi[i] < 0.) {
                    yi[i] = 0.;
                }
            } else {
                yi[i] = y[0];
            }
        } else {
            int loc = binarySearch(x, xi[i]);
            if (loc < -1) {
                loc = -loc - 2;
                yi[i] = slope[loc] * xi[i] + intercept[loc];
            } else {
                yi[i] = y[loc];
            }
        }
    }

    delete dx;
    delete dy;
    delete slope;
    delete intercept;

    return yi;
}

std::vector<double> inter_t_var
(const std::vector<double>& tm,
 const std::vector<double>& vols,
 const std::vector<double>& tTargetGrid) {

    bool linearExtrapolation = false;
    if (vols.size() != tm.size()) {
        throw std::runtime_error("error");
    }

    if (tm.size() == 1) {
        std::vector<double> returnVols =
            std::vector<double>(tTargetGrid.size());

        for (int i = 0; i < tTargetGrid.size(); i++) {
            returnVols[i] = vols[0];
        }
        return returnVols;
    } else {
        std::vector<double> vars = std::vector<double>(tm.size());
        for (int i = 0; i < tm.size(); i++) {
            vars[i] = pow(vols[i], 2.0) * tm[i];
        }
        std::vector<double> varsInterp =
            interpLinear(tm, vars, tTargetGrid, linearExtrapolation);
        for (int i = 0; i < varsInterp.size(); i++) {
            if (tTargetGrid[i] < tm[0]) {
                varsInterp[i] = sqrt(varsInterp[i] / tm[0]);
            } else if (tTargetGrid[i] > tm[tm.size() - 1]) {
                varsInterp[i] =
                    sqrt(varsInterp[i] / tm[tm.size() - 1]);
            } else {
                varsInterp[i] = sqrt(varsInterp[i] / tTargetGrid[i]);
            }
        }
        return varsInterp;
    }
}

std::vector<double> inter_x_var
(const std::vector<double>& x,
 const std::vector<double>& vols,
 const std::vector<double>& xTargetGrid) {

    bool linearExtrapolation = true;
    if (vols.size() != x.size()) {
        throw std::runtime_error("error");
    }
    if (x.size() == 1) {
        std::vector<double> returnVols =
            std::vector<double>(xTargetGrid.size());

        for (int i = 0; i < xTargetGrid.size(); i++) {
            returnVols[i] = vols[0];
        }
        return returnVols;
    } else {
        std::vector<double> vars = std::vector<double>(x.size());
        for (int i = 0; i < x.size(); i++) {
            vars[i] = pow(vols[i], 2.0);
        }
        std::vector<double> varsInterp =
            interpLinear(x, vars, xTargetGrid, linearExtrapolation);
        for(int i = 0; i < varsInterp.size(); i++) {
            varsInterp[i] = sqrt(varsInterp[i]);
        }
        return varsInterp;
    }
}

std::vector<std::vector<double> > interp_in_x_then_t
(const std::vector<std::vector<double> >& inputVolGrid,
 const std::vector<double>& t,
 const std::vector<double>& x,
 const std::vector<double>& tTargetGrid,
 const std::vector<double>& xTargetGrid) {

    std::vector<std::vector<double> > x_Interped;

    for(int it = 0; it < inputVolGrid.size(); it++) {
        const std::vector<double>&
        input_vols_for_given_t =
            inputVolGrid[it];

        x_Interped.push_back
        (
            inter_x_var(x,
                        input_vols_for_given_t,
                        xTargetGrid)
        );
    }

    std::vector<std::vector<double> > x_Interped_tran
        = transpose(x_Interped);

    std::vector<std::vector<double> > x_t_Interped_tran;

    for(int ix = 0; ix < x_Interped_tran.size(); ix++) {
        const std::vector<double>&
        input_vols_for_given_x =
            x_Interped_tran[ix];

        x_t_Interped_tran.push_back
        (
            inter_t_var(t,
                        input_vols_for_given_x,
                        tTargetGrid)
        );
    }

    return transpose(x_t_Interped_tran);
}


double CumulativeNormal(double x)
{
    double y, Exponential, SumA, SumB, ret;

    y = fabs(x);

    if(y > 37.)
    {
        ret = 0.;
    } else
    {
        Exponential = exp(-pow(y,2.) / 2.);
        if(y < 7.07106781186547)
        {
            SumA = 0.0352624965998911*y + 0.700383064443688;
            SumA = SumA * y + 6.37396220353165;
            SumA = SumA * y + 33.912866078383;
            SumA = SumA * y + 112.079291497871;
            SumA = SumA * y + 221.213596169931;
            SumA = SumA * y + 220.206867912376;
            SumB = 0.0883883476483184 * y + 1.75566716318264;
            SumB = SumB * y + 16.064177579207;
            SumB = SumB * y + 86.7807322029461;
            SumB = SumB * y + 296.564248779674;
            SumB = SumB * y + 637.333633378831;
            SumB = SumB * y + 793.826512519948;
            SumB = SumB * y + 440.413735824752;
            ret = Exponential * SumA / SumB;
        } else {
            SumA = y + 0.65;
            SumA = y + 4. / SumA;
            SumA = y + 3. / SumA;
            SumA = y + 2. / SumA;
            SumA = y + 1. / SumA;
            ret = Exponential / (SumA * 2.506628274631);
        }
    }

    if(x > 0.)
    {
        ret = 1. - ret;
    }

    return ret;
}




double option_price_european_payout( const double& S
                                     ,const double& X
                                     ,const double& r
                                     ,const double& q
                                     ,const double& sigma
                                     ,const double& time
                                     ,std::string strType
                                     ,std::string formulaType
                                   )
{
    double price;
    double sigma_sqr = pow(sigma,2);
    double time_sqrt = sqrt(time);
    double l_r, l_q;
    double lag;

    l_r = r;
    l_q = q;
    lag = 0.;

    l_q = 0.;
    l_r = 0.;

    double d1 = (log(S/X) + (l_r-l_q + 0.5*sigma_sqr)*time)/(sigma*time_sqrt);
    double d2 = d1-(sigma*time_sqrt);

    if(strType == "call")
    {
        price = S * exp(-l_q*(time+lag))* CumulativeNormal(d1) - X * exp(-l_r*(time+lag)) * CumulativeNormal(d2);
    } else if(strType == "put")
    {
        price = X * exp(-l_r*(time+lag)) * CumulativeNormal(-d2) -  S * exp(-l_q*(time+lag))* CumulativeNormal(-d1);
    }

    return price;
}



std::string lowerCase(const std::string& arg)
{
    std::string arg_local = arg;
    std::string::iterator StringValueIterator = arg_local.begin();
    std::transform(arg_local.begin(), arg_local.end(), StringValueIterator, ::tolower);
    return arg_local;
}


extern "C"
int cMatrix_setRow(cMatrix* p, double* pRow, ui ix,
                   ui lengthRow) {
    if(p == 0) {
        return ERROR;
    }
    if(lengthRow != p->sizey) {
        return ERROR;
    }
    if(ix >= p->sizex) {
        return ERROR;
    }

    for(ui j=0; j<lengthRow; j++) {
        p->table[ix][j] =pRow[j];
    }
    return 0;
}

extern "C"
int cIntArr_set(cIntArr* p, int* pArr, ui lenArr) {
    if(p == 0) {
        return ERROR;
    }
    if(lenArr != p->size) {
        return ERROR;
    }

    for(ui j=0; j<lenArr; j++) {
        p->iarr[j] =pArr[j];
    }
    return 0;
}

extern "C"
int cDblArr_set(cDblArr* p, double* pArr, ui lenArr) {
    if(p == 0) {
        return ERROR;
    }
    if(lenArr != p->size) {
        return ERROR;
    }

    for(ui j=0; j<lenArr; j++) {
        p->darr[j] =pArr[j];
    }
    return 0;
}

extern "C"
void cProcessATMVols(cIntArr* dts, cDblArr* tm, cMatrix* vols,
                     cTimeSeries *results) {

    if(results->tm != NULL ||
            results->dates != NULL ||
            results->vols != NULL) {
        throw std::runtime_error("error");
    }

    int numDts = dts->size;
    int numTm = tm->size;

    if(numDts != vols->sizex) {
        return;
    }

    if(numTm != vols->sizey) {
        return;
    }

    std::vector<std::pair<int, std::vector<double> > >
    vAtmGrid;

    for(int r=0; r<numDts; r++) {
        std::vector<double> rowOfVols;
        for(int c=0; c<numTm; c++) {
            rowOfVols.push_back(vols->table[r][c]);
        }
        std::pair<int, std::vector<double> > pr =
            std::make_pair<int,std::vector<double> >
            (dts->iarr[r], rowOfVols);
        vAtmGrid.push_back(pr);
    }

    xlw::CellMatrix atmGrid =
        xlw::CellMatrix().getCellMatrixFrom_dtdbpairvec(vAtmGrid);

    std::sort(atmGrid.begin(),
              atmGrid.end(),
              xlw::CellMatrix::reverseCompareDataMatrix);

    int dateOfSimulation = atmGrid(0,0).NumericValue();
    int hp = 2;
    int numScen = 1250;
    int seedVolN = 60;
    xlw::MyArray lambdaArr;
    lambdaArr.push_back(0.97);
    std::string method = "relative";
    bool useAntithetic = false;
    double percentile = 0.997;
    int retScal = 1;
    bool ewmaFrom1Day = false;
    bool useMaxReturns = false;
    bool useLegacy = true;
    bool useOneSetofEWMA = true;
    bool scaledMargins = false;
    bool scaleRiskFactorsSeparately = true;
    int lastestEWMAScalingFunctionalForm = 0;
    int numVolsLT = 100;
    int numVolsP = 100;
    double volPercentile = 0.3;
    bool aggregate1DScaledReturns = false;

    if(atmGrid.checkDecreasingOrder() == false) {
        throw std::runtime_error("error");
    }

    if(hp < 1)
    {
        throw std::runtime_error("error");
    }

    if(numScen < 1)
    {
        throw std::runtime_error("error");
    }

    if(seedVolN < 1)
    {
        throw std::runtime_error("error");
    }

    method = lowerCase(method);

    if(method != "relative" && method != "absolute")
    {
        throw std::runtime_error("error");
    }

    if(percentile <= 0. || percentile >= 1.0)
    {
        throw std::runtime_error("error");
    }

    xlw::dtdbpairvec atmGridAsPair = atmGrid.getAsPairArray();

    constructSR
    varSim(hp,
           numScen,
           useOneSetofEWMA,
           atmGridAsPair,
           ewmaFrom1Day,
           scaledMargins,
           seedVolN,
           lambdaArr,
           method,
           useAntithetic,
           percentile,
           retScal,
           useMaxReturns,
           useLegacy,
           lastestEWMAScalingFunctionalForm,
           numVolsLT,
           numVolsP,
           volPercentile,
           aggregate1DScaledReturns);

    std::vector<xlw::dtdbpairvec> returnByHoldingPeriod =
        varSim.getRArrRef();

    xlw::dtdbpairvec ewmaVols = varSim.getE();

    std::vector<xlw::dtdbpairvec> sR(hp);

    varSim.setSR
    (returnByHoldingPeriod[hp-1],
     ewmaVols,
     retScal,
     dateOfSimulation,
     ewmaFrom1Day,
     hp,
     useLegacy,
     numScen,
     sR);

    xlw::dtdbpairvec scaledRets = sR[hp-1];

    int retNumDates = scaledRets.size();
    int retNumFactors = scaledRets[0].second.size();

    std::vector<int> retDates(retNumDates);
    std::vector<std::vector<double> > retVols(retNumDates);

    for(int i=0; i<retNumDates; i++) {
        retDates[i] = scaledRets[i].first;
        retVols[i] = scaledRets[i].second;
    }

    cIntArr* pRetDates = NULL;

    int success = allocate_from_vectorInt(pRetDates,retDates);
    if(success == ERROR) {
        return;
    }

    cDblArr* pRetTm = NULL;
    success = copyDblArr(pRetTm, tm);
    if(success == ERROR) {
        cIntArr_free(pRetDates);
        return;
    }

    cMatrix* pRetVols = NULL;
    success = allocate_from_matrix(pRetVols, retVols);
    if(success == ERROR) {
        cIntArr_free(pRetDates);
        cDblArr_free(pRetTm);
        return;
    }

    results->tm = pRetTm;
    results->dates = pRetDates;
    results->vols = pRetVols;
}

extern "C"
void cCalculateOptionScenPrices
(double futurePrice, double strike, double tm,
 int callOrPut,
 int stirOrNot,
 cIntArr* scenDates, cDblArr* scenFutPrices,
 cIntArr* atmScenVol_dts, cDblArr* atmScenVol_tm,
 cMatrix* atmScenVol_vols, cDblArr* volGrid_tm,
 cDblArr* volGrid_lnks, cMatrix* volGrid_vols,
 cOptionScenPrices* results) {

    std::string optionType;
    if(callOrPut == 1) {
        if(stirOrNot == 1) {
            optionType = "put";
        } else {
            optionType = "call";
        }
    } else {
        if(stirOrNot == 1) {
            optionType = "call";
        } else {
            optionType = "put";
        }
    }

    std::string formulaType = "asay";

    std::vector<int> vScenDates =
        copy1DArrayToVector(scenDates->size,scenDates->iarr);
    std::vector<double> vScenFutPrices =
        copy1DArrayToVector(scenFutPrices->size,scenFutPrices->darr);

    int N = vScenDates.size();

    std::vector<std::pair<int, std::vector<double> > >
    scen(N);
    for(int i=0; i<N; i++) {
        std::vector<double> v;
        v.push_back(vScenFutPrices[i]);
        scen[i] = std::make_pair<int,std::vector<double> >
                  (vScenDates[i],v);
    }
    xlw::CellMatrix scenCell = xlw::CellMatrix().getCellMatrixFrom_dtdbpairvec(scen);
    std::sort(scenCell.begin(),
              scenCell.end(),
              xlw::CellMatrix::reverseCompareDataMatrix);
    for(int i=0; i<N; i++) {
        vScenDates[i] = scenCell(i,0).NumericValue();
        vScenFutPrices[i] = scenCell(i,1).NumericValue();
    }

    std::vector<int> vAtmScenVol_dts =
        copy1DArrayToVector(atmScenVol_dts->size,atmScenVol_dts->iarr);
    std::vector<double> vAtmScenVol_tm =
        copy1DArrayToVector(atmScenVol_tm->size,atmScenVol_tm->darr);
    std::vector<std::vector<double> > vAtmScenVol_vols =
        copy2DArrayToVector(atmScenVol_vols->sizex,
                            atmScenVol_vols->sizey,
                            atmScenVol_vols->table);

    std::vector<double> vVolGrid_tm =
        copy1DArrayToVector(volGrid_tm->size,volGrid_tm->darr);
    std::vector<double> vVolGrid_lnks =
        copy1DArrayToVector(volGrid_lnks->size,volGrid_lnks->darr);
    std::vector<std::vector<double> > vVolGrid_vols =
        copy2DArrayToVector(volGrid_vols->sizex,
                            volGrid_vols->sizey,
                            volGrid_vols->table);

    int N_tm = vAtmScenVol_tm.size();
    int N_lnks = vVolGrid_lnks.size();
    std::vector<double> interpVols(N);
    std::vector<double> scenOptionPrices(N);

    if(vAtmScenVol_dts.size() < N) {
        throw std::runtime_error("error");
    }

    if(N_tm != vVolGrid_tm.size()) {
        throw std::runtime_error("error");
    }

    double scaledReturn;
    double lnk = log(strike/futurePrice);
    std::vector<double> vlnk;
    vlnk.push_back(lnk);
    std::vector<double> vtm;
    vtm.push_back(tm);

    std::vector<std::vector<double> > interpedVol
        = interp_in_x_then_t(vVolGrid_vols,vVolGrid_tm,vVolGrid_lnks,
                             vtm, vlnk);
    double baseOptionPrice;
    double F, K;

    if(stirOrNot == 1) {
        F = 100 - futurePrice;
        K = 100 - strike;
    } else {
        F = futurePrice;
        K = strike;
    }

    if(F < 0.) {
        F = 0.0000001;
    }
    if(K < 0.) {
        K = 0.0000001;
    }

    baseOptionPrice = option_price_european_payout(F,K,0,0,interpedVol[0][0]/100.0,tm,optionType,formulaType);

    results->interpolatedBaseVol = interpedVol[0][0];

    for(int i=0; i<N; i++) {
        lnk = log(strike/vScenFutPrices[i]);
        vlnk[0] = lnk;
        std::vector<std::vector<double> >
        scenVolGrid = vVolGrid_vols;
        for(int t=0; t<N_tm; t++) {
            scaledReturn = vAtmScenVol_vols[i][t];
            for(int k=0; k<N_lnks; k++) {
                scenVolGrid[t][k] *= (1+std::max(std::min(scaledReturn,0.5),-0.5));
            }
        }
        std::vector<std::vector<double> > interpedVol
            = interp_in_x_then_t(scenVolGrid,vVolGrid_tm,vVolGrid_lnks,
                                 vtm, vlnk);
        interpVols[i] = interpedVol[0][0];

        double sigma = interpVols[i]/100.;

        if(stirOrNot == 1) {
            F = 100 - vScenFutPrices[i];
        } else {
            F = vScenFutPrices[i];
        }

        if(F < 0.) {
            F = 0.0000001;
        }

        if(K < 0.) {
            K = 0.0000001;
        }

        double scenPrice = option_price_european_payout(F,K,0,0,interpedVol[0][0]/100.0,tm,optionType,formulaType);

        scenOptionPrices[i] = scenPrice;
    }

    cIntArr* pDates = NULL;
    int success = allocate_from_vectorInt(pDates,vScenDates);
    if(success == ERROR) {
        std::cout<<"error\n";
        cIntArr_free(pDates);
        return;
    }

    cDblArr* pVols = NULL;
    success = allocate_from_vectorDbl(pVols, interpVols);
    if(success == ERROR) {
        std::cout<<"error\n";
        cIntArr_free(pDates);
        cDblArr_free(pVols);
        return;
    }

    cDblArr *pScenContractPrices = NULL;
    success = allocate_from_vectorDbl(pScenContractPrices, vScenFutPrices);
    if(success == ERROR) {
        std::cout<<"error\n";
        cIntArr_free(pDates);
        cDblArr_free(pVols);
        cDblArr_free(pScenContractPrices);
        return;
    }

    cDblArr *pScenOptionPrices = NULL;
    success = allocate_from_vectorDbl(pScenOptionPrices,
                                      scenOptionPrices);
    if(success == ERROR) {
        std::cout<<"error\n";
        cIntArr_free(pDates);
        cDblArr_free(pVols);
        cDblArr_free(pScenContractPrices);
        cDblArr_free(pScenOptionPrices);
        return;
    }

    results->contractPrice = futurePrice;
    results->strike = strike;
    results->tm = tm;
    results->callOrPut = callOrPut;
    results->stirOrNot = stirOrNot;
    results->optionPrice = baseOptionPrice;
    results->dates = pDates;
    results->vols = pVols;
    results->scenContractPrices = pScenContractPrices;
    results->scenOptionPrices = pScenOptionPrices;
}
