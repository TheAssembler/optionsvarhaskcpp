#ifndef __DATASTRUCTURE_
#define __DATASTRUCTURE_

#include <stdint.h>

typedef struct {
  uint32_t sizex;
  uint32_t sizey;
  double **table;
} cMatrix;

typedef struct {
  uint32_t size;
  int *iarr;
} cIntArr;

typedef struct {
  uint32_t size;
  double *darr;
} cDblArr;

typedef struct {
  cDblArr *tm;
  cIntArr *dates;
  cMatrix *vols;
} cTimeSeries;

typedef struct {
  double contractPrice;
  double strike;
  double tm;
  int callOrPut;
  int stirOrNot;
  double optionPrice;
  double interpolatedBaseVol;
  cIntArr *dates;
  cDblArr *vols;
  cDblArr *scenContractPrices;
  cDblArr *scenOptionPrices;
} cOptionScenPrices;


#endif
