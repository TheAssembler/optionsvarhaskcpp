//
//
//                        CellMatrix.cpp
//
//
/*
Copyright (C) 2006 Mark Joshi
Copyright (C) 2007 Tim Brunne
Copyright (C) 2007, 2008 Eric Ehlers
Copyright (C) 2009 Narinder S Claire

This file is part of XLW, a free-software/open-source C++ wrapper of the
Excel C API - http://xlw.sourceforge.net/

XLW is free software: you can redistribute it and/or modify it under the
terms of the XLW license.  You should have received a copy of the
license along with this program; if not, please email xlw-users@lists.sf.net

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the license for more details.
*/
#include "CellMatrix.h"
#include <algorithm>
#include <stdexcept>
namespace{//unnamed namespace to restrict to file scope
	size_t maxi(size_t a, size_t b)
	{
		return a > b ? a : b;
	}
}

namespace xlw{
	bool CellValue::IsAString() const
	{
		return Type == string;
	}


	bool CellValue::IsAWstring() const
	{
		return Type == wstring;
	}

	bool CellValue::IsANumber() const
	{
		return Type == number;
	}

	bool CellValue::IsBoolean() const
	{
		return Type == boolean;
	}

	bool CellValue::IsXlfOper() const
	{
		return Type == xlfoper;
	}

	bool CellValue::IsError() const
	{
		return Type == error;
	}

	bool CellValue::IsEmpty() const
	{
		return Type == empty;
	}


	CellValue::operator std::string() const
	{
		if (Type != string)
			throw("non string cell asked to be a string");
		return ValueAsString;
	}

	CellValue::operator std::wstring() const
	{
		if (Type != wstring)
			throw("non string cell asked to be a string");
		return ValueAsWstring;
	}

	CellValue::operator bool() const
	{
		if (Type != boolean)
			throw("non boolean cell asked to be a bool");
		return ValueAsBool;
	}

	CellValue::operator double() const
	{
		if (Type != number)
			throw("non number cell asked to be a number");
		return ValueAsNumeric;
	}

	CellValue::operator unsigned long() const
	{
		if (Type != number)
			throw("non number cell asked to be a number");
		return static_cast<unsigned long>(ValueAsNumeric);
	}

	void CellValue::clear()
	{
		Type = empty;
		ValueAsString="";
		ValueAsWstring=L"";
		ValueAsNumeric=0.0;
		ValueAsBool=false;
		ValueAsErrorCode=0;
	}

	bool CellValue::operator==(const CellValue& arg) const
	{
		if(Type == arg.Type && Type == string)
		{
			if(this->StringValue() == arg.StringValue())
			{
				return true;
			}else
			{
				return false;
			}
		}else if(Type == arg.Type && Type == wstring)
		{
			if(this->WstringValue() == arg.WstringValue())
			{
				return true;
			}else
			{
				return false;
			}
		}
		else if(Type == arg.Type && Type == number)
		{
			if(this->NumericValue() == arg.NumericValue())
			{
				return true;
			}else
			{
				return false;
			}
		}
		else if(Type == arg.Type && Type == boolean)
		{
			if(this->BooleanValue() == arg.BooleanValue())
			{
				return true;
			}else
			{
				return false;
			}
		}
		else
		{
			throw std::runtime_error("CellValue : comparison not supported for this type");
		}
	}

	CellValue::CellValue(const std::string& value) : Type(CellValue::string),
		ValueAsString(value), ValueAsWstring(L""), ValueAsNumeric(0.0), ValueAsBool(false), ValueAsErrorCode(0)
	{

	}

	CellValue::CellValue(const std::wstring& value) : Type(CellValue::wstring),
		ValueAsString(""), ValueAsWstring(value), ValueAsNumeric(0.0), ValueAsBool(false), ValueAsErrorCode(0)
	{

	}

	CellValue::CellValue(const char* value) : Type(CellValue::string),
		ValueAsString(value), ValueAsWstring(L""), ValueAsNumeric(0.0), ValueAsBool(false), ValueAsErrorCode(0)
	{

	}
	CellValue::CellValue(double Number): Type(CellValue::number),
		ValueAsString(""), ValueAsWstring(L""), ValueAsNumeric(Number), ValueAsBool(false), ValueAsErrorCode(0)
	{

	}

	CellValue::CellValue(int i): Type(CellValue::number),
		ValueAsString(""), ValueAsWstring(L""), ValueAsNumeric(i), ValueAsBool(false), ValueAsErrorCode(0)
	{

	}

	CellValue::CellValue(unsigned long Code, bool Error): Type(error),
		ValueAsString(""), ValueAsWstring(L""), ValueAsNumeric(Code), ValueAsBool(false), ValueAsErrorCode(Code)
	{
		if (!Error)
			Type = number;
	}

	CellValue::CellValue(bool TrueFalse)
		: Type(CellValue::boolean),
		ValueAsString(""), ValueAsWstring(L""), ValueAsNumeric(0.0), ValueAsBool(TrueFalse), ValueAsErrorCode(0)
	{
	}

	CellValue::CellValue(): Type(CellValue::empty),
		ValueAsString(""), ValueAsWstring(L""), ValueAsNumeric(0.0), ValueAsBool(false), ValueAsErrorCode(0)
	{
	}

	std::string CellValue::StringValue() const
	{
		if (Type == string) {
			return ValueAsString;
		} else if (Type == wstring) {
			return std::string(ValueAsWstring.begin(), ValueAsWstring.end());
		} else {
			throw("non string cell asked to be a string");
		}
	}

	const std::wstring& CellValue::WstringValue() const
	{
		if (Type != wstring)
			throw("non wstring cell asked to be a wstring");
		return ValueAsWstring;
	}

	double CellValue::NumericValue() const
	{

		if (Type != number)
			throw("non number cell asked to be a number");
		return ValueAsNumeric;
	}

	bool CellValue::BooleanValue() const
	{

		if (Type != boolean)
			throw("non boolean cell asked to be a bool");

		return ValueAsBool;
	}

	unsigned long CellValue::ErrorValue() const
	{
		if (Type != error)
			throw("non error cell asked to be an error");

		return ValueAsErrorCode;
	}

	std::string CellValue::StringValueLowerCase() const
	{
		if (Type == string) {
			std::string tmp(ValueAsString);
			std::transform(tmp.begin(),tmp.end(),tmp.begin(),tolower);
			return tmp;
		} else if (Type == wstring) {
			std::wstring w(WstringValueLowerCase());
			return std::string(w.begin(), w.end());
		} else {
			throw("non string cell asked to be a string");
		}
	}

	std::wstring CellValue::WstringValueLowerCase() const
	{
		if (Type == string) {
			std::string s(StringValueLowerCase());
			return std::wstring(s.begin(), s.end());
		} else if (Type == wstring) {
			std::wstring tmp(ValueAsWstring);
			std::transform(tmp.begin(),tmp.end(),tmp.begin(),tolower);
			return tmp;
		} else {
			throw("non string cell asked to be a string");
		}
	}


	int CellValue::getType(void) const
	{
		return Type;
	}
  
        int CellMatrix::colToCompare = 0;

	CellMatrix::CellMatrix() : Cells(0), Rows(0), Columns(0)
	{
	}


	CellMatrix::CellMatrix(double x): Cells(1), Rows(1), Columns(1)
	{
		Cells[0].push_back(CellValue(x));
	}

	CellMatrix::CellMatrix(std::string x): Cells(1), Rows(1), Columns(1)
	{
		Cells[0].push_back(CellValue(x));
	}  

	CellMatrix::CellMatrix(std::wstring x): Cells(1), Rows(1), Columns(1)
	{
		Cells[0].push_back(CellValue(x));
	}

	CellMatrix::CellMatrix(const char* x): Cells(1), Rows(1), Columns(1)
	{
		Cells[0].push_back(CellValue(x));
	}

	CellMatrix::CellMatrix(const MyArray& data) : Cells(data.size()),
		Rows(data.size()), Columns(1)
	{
		for (size_t i=0; i < data.size(); ++i)
			Cells[i].push_back(CellValue(data[i]));
	}

	CellMatrix::CellMatrix(const MyMatrix& data): Cells(data.size1()),
		Rows(data.size1()), Columns(data.size2())
	{
		for (size_t i=0; i < data.size1(); ++i)
			for (size_t j=0; j < data.size2(); ++j)
				Cells[i].push_back(CellValue(Element(data,i,j)));
	}

	CellMatrix::CellMatrix(unsigned long i)
		: Cells(1), Rows(1), Columns(1)
	{
		Cells[0].push_back(CellValue(static_cast<double>(i)));
	}

	CellMatrix::CellMatrix(int i): Cells(1), Rows(1), Columns(1)
	{
		Cells[0].push_back(CellValue(static_cast<double>(i)));
	}

	CellMatrix::CellMatrix(const std::vector<CellValue>& i_Cells)
	{
		Cells.clear();
		Cells.push_back(i_Cells);

		Rows = Cells.size();
		Columns = Cells[0].size();
	}

	CellMatrix::CellMatrix(const std::vector<int>& data) : Cells(data.size()),
		Rows(data.size()), Columns(1)
	{
		for (size_t i=0; i < data.size(); ++i)
			Cells[i].push_back(CellValue(data[i]));
	}

	CellMatrix::CellMatrix(size_t rows, size_t columns)
		: Cells(rows), Rows(rows), Columns(columns)
	{
		for (size_t i=0; i < rows; i++)
			Cells[i].resize(columns);
	}

	const CellValue& CellMatrix::operator()(size_t i, size_t j) const
	{
		return Cells.at(i).at(j);

	}

	CellValue& CellMatrix::operator()(size_t i, size_t j)
	{
		return Cells.at(i).at(j);
	}

	MyArray CellMatrix::getAsVectorArray(void) const
	{
		MyArray ret;

		for(int i=0; i<(int)Rows; ++i)
		{
			ret.push_back(this->operator()(i,0));
		}

		return ret;
	}

	dtdbpairvec CellMatrix::getAsPairArray(void) const
	{
		dtdbpairvec ret;
		int first, numFactors, col;

		numFactors = int(this->ColumnsInStructure());

		for(int i=0; i<(int)Rows; ++i)
		{
			std::vector<double> second;
			first = int(this->operator()(i,0).NumericValue());
			for(col = 1; col < numFactors; ++col){
				second.push_back(this->operator()(i,col).NumericValue());
			}
			ret.push_back(std::make_pair<int,std::vector<double> >(first, second));
		}

		return ret;
	}

	size_t CellMatrix::RowsInStructure() const
	{
		return Rows;
	}
	size_t CellMatrix::ColumnsInStructure() const
	{
		return Columns;
	}

	std::vector<std::vector<CellValue> >::iterator CellMatrix::begin()
	{
		return Cells.begin();
	}

	std::vector<std::vector<CellValue> >::iterator CellMatrix::end()
	{
		return Cells.end();
	}

	const std::vector<CellValue>& CellMatrix::operator[](unsigned i) const
	{
		return Cells[i];
	}

	std::vector<CellValue>& CellMatrix::operator[](unsigned i)
	{
		return Cells[i];
	}

	const xlw::CellMatrix CellMatrix::getCellMatrixRow(unsigned i) const
	{
		CellMatrix ret(ColumnsInStructure(),1);
		for(int col=0; col<(int)ColumnsInStructure();++col)
		{
			ret(col,0) = Cells[i][col]; 
		}
		return ret;
	}


	xlw::CellMatrix CellMatrix::getCellMatrixRow(unsigned i)
	{
		CellMatrix ret(ColumnsInStructure(),1);
		for(int col=0; col<(int)ColumnsInStructure();++col)
		{
			ret(col,0) = Cells[i][col]; 
		}
		return ret;			
	}

	xlw::CellMatrix CellMatrix::getCellMatrixFrom_dtdbpairvec(const dtdbpairvec& arg)
	{	
		int sizeOfVec = arg.size();
		int numFactors = arg[0].second.size();
		CellMatrix ret(sizeOfVec, 1+numFactors);

		for(int j=0; j<numFactors; ++j){
			for(int i=0; i<sizeOfVec; ++i){
				ret(i,0) = arg[i].first;
				ret(i,1+j) = arg[i].second[j];
			}
		}

		return ret;
	}

	void CellMatrix::clear()
	{
		Cells.clear();
	}

	bool CellMatrix::compareDataMatrixInts(const std::vector<CellValue>& a, 
		const std::vector<CellValue>& b)
	{
		if(a.size()==0 || b.size() == 0)
		{
			throw std::runtime_error("CellMatrix::compareDataMatrixInts : error");
		}else
		{
			return ((int)a[0].NumericValue() < (int)b[0].NumericValue());
		}
		return true;
	}
  
	bool CellMatrix::compareDataMatrixForCol(const std::vector<CellValue>& a, 
		const std::vector<CellValue>& b)
	{
		if(a.size()==0 || b.size() == 0)
		{
			throw std::runtime_error("CellMatrix::compareDataMatrixForCol : error");
		}else
		{
			return (a[colToCompare].NumericValue() < b[colToCompare].NumericValue());
		}
		return true;
                }

	std::string CellMatrix::lowerCase(const std::string& arg){
		std::string arg_local = arg;
		std::string::iterator StringValueIterator = arg_local.begin();
		std::transform(arg_local.begin(), arg_local.end(), StringValueIterator, tolower);
		return arg_local;
	}
  
	void CellMatrix::sort(int col)
	{
		colToCompare = col;
		std::sort(Cells.begin(),Cells.end(),CellMatrix::compareDataMatrixForCol);
                }

	bool CellMatrix::reverseCompareDataMatrix(const std::vector<CellValue>& a, 
		const std::vector<CellValue>& b)
	{
		return !compareDataMatrixInts(a,b);
		return true;
	}

	CellMatrix MergeCellMatrices(const CellMatrix& Top, const CellMatrix& Bottom)
	{
		size_t cols = maxi(Top.ColumnsInStructure(), Bottom.ColumnsInStructure());
		size_t rows = Top.RowsInStructure()+Bottom.RowsInStructure();

		CellMatrix merged(rows,cols);

		{for (size_t i=0; i < Top.ColumnsInStructure(); i++)
			for (size_t j=0; j < Top.RowsInStructure(); j++)
				merged(j,i) = Top(j,i);}

		for (size_t i=0; i < Bottom.ColumnsInStructure(); i++)
			for (size_t j=0; j < Bottom.RowsInStructure(); j++)
				merged(j+Top.RowsInStructure(),i) = Bottom(j,i);

		return merged;
	}

	void CellMatrix::pop_front()
	{
		std::vector<std::vector<CellValue> > newCells;
		//We exclude the first row
		for(int i=1; i<(int)Rows; ++i)
		{
			newCells.push_back(Cells[i]);
		}
		Cells = newCells;
		Rows = Cells.size();
	}

	void CellMatrix::pop_back()
	{
		Cells.pop_back();
		Rows = Cells.size();
	}

	void CellMatrix::push_front(const CellMatrix& arg)
	{
		int arg_cols = arg.ColumnsInStructure();
		if(arg_cols != Columns)
		{
			throw std::runtime_error("CellMatrix::push_front : trying to push a CellMatrix in the front with a different number of columns to current CellMatrix");
		}

		CellMatrix ret(arg);	
		ret.PushBottom(*this);
		*this = ret;
		Rows = Cells.size();
	}

	/*!
	This does the same as PushBottom except that it ensures that the structure being added on the 
	bottom has the same number of columns.
	*/
	void CellMatrix::push_back(const CellMatrix& arg)
	{
		int argCols = arg.ColumnsInStructure();

		if(argCols != Columns)
		{
			throw std::runtime_error("CellMatrix::push_back: matrix being pushed on the bottom does not have the same number of columns");
		}

		this->PushBottom(arg);
		Rows = Cells.size();
	}

	void CellMatrix::PushBottom(const CellMatrix& newRows)
	{
		CellMatrix newRowsResize(newRows);
		size_t newColumns = maxi(newRows.ColumnsInStructure(),Columns);

		if (newColumns > Columns)
			for (size_t i=0; i < Rows; i++)
				Cells[i].resize(newColumns);

		if (newColumns > newRows.Columns)
			for (size_t i=0; i < newRowsResize.Rows; i++)
				newRowsResize.Cells[i].resize(newColumns);

		for (size_t i=0; i < newRowsResize.Rows; i++)
			Cells.push_back(newRowsResize.Cells[i]);

		Rows = static_cast<size_t>(Cells.size());
		Columns = newColumns;
	}

	const CellMatrix CellMatrix::GetTranspose(void) const
	{
		CellMatrix transposeMat(Columns,Rows);

		for(int x=0; x<(int)Rows; ++x)
		{
			for(int y=0; y<(int)Columns;++y)
			{
				transposeMat(y,x) = this->operator()(x,y);
			}
		}

		return transposeMat;
	}
	//Note that this could have been implemented much more simply
	//by defining the appropriate CellMatrix, and manually copying the
	//elements. However we did it this way so that at a later time
	//we could implement these methods on the current CellMatrix
	const CellMatrix CellMatrix::RemoveCol(int i_col) const
	{	
		if(i_col >= (int)Columns)
		{
			std::string msg;
			msg = "CellMatrix:RemoveRow : col must be a value between 0 and ";
			msg += Columns-1;
			throw std::runtime_error(msg.c_str());
		}

		int col = 0, row = 0;
		CellMatrix ret = *this;
		std::vector<CellValue>::iterator pCol;
		std::vector<std::vector<CellValue> >::iterator pRow;

		for(row=0; row<(int)Rows;++row)
		{
			pRow = ret.Cells.begin();

			for(int k=0; k<row; ++k)
			{
				++pRow;
				//arjun : I think that we have to redo this after each and every
				//erase operation as the iterators are invalidated. Check this.
			}

			pCol = pRow->begin();

			col = 0;
			while(col < i_col)
			{
				++col;
				++pCol;
			}
			pRow->erase(pCol);
		}
		ret.Columns = ret.Columns - 1;
		return ret;
	}

	//Note that this could have been implemented much more simply
	//by defining the appropriate CellMatrix, and manually copying the
	//elements. However we did it this way so that at a later time
	//we could implement these methods on the current CellMatrix
	const CellMatrix CellMatrix::RemoveRow(int i_row) const
	{
		if(i_row >= (int)Rows)
		{
			std::string msg;
			msg = "CellMatrix:RemoveRow : row must be a value between 0 and ";
			msg += Rows-1;
			throw std::runtime_error(msg.c_str());
		}

		int row = 0;
		CellMatrix ret = *this;
		std::vector<std::vector<CellValue> >::iterator p;
		p = ret.Cells.begin();

		while(row<i_row)
		{
			++p;
			row++;
		}

		ret.Cells.erase(p);
		ret.Rows = ret.Rows - 1;
		return ret;
	}


	void CellMatrix::ResizeUponNonData(void)
	{
		int num_row_in_resized, num_col_in_resized;

		//num_row_in_resized = RowsInStructure();
		//num_col_in_resized = ColumnsInStructure();

		num_row_in_resized = 0;
		num_col_in_resized = 0;

		if(Rows == 0 && Columns == 0)
		{
			throw std::runtime_error("CellMatrix : trying to resize zero size matrix");
		}

		for(int x=0; x<(int)RowsInStructure(); ++x)
		{
			for(int y=0; y<(int)ColumnsInStructure(); ++y)
			{		
				if(Cells[x][y].IsANumber() || Cells[x][y].IsAString() || Cells[x][y].IsAWstring())
				{
					if(y>num_col_in_resized-1)
					{
						num_col_in_resized=y+1;
					}

					if(x>num_row_in_resized-1)
					{
						num_row_in_resized=x+1;
					}
				}
			}
		}

		std::vector<std::vector<CellValue> >::iterator p_CellMat;
		std::vector<CellValue>::iterator p_row;
		int numRowsToDelete = RowsInStructure() - num_row_in_resized;

		//Deleting the rows
		while(numRowsToDelete)
		{
			p_CellMat = Cells.end();
			p_CellMat--;
			Cells.erase(p_CellMat);
			numRowsToDelete--;
		}

		int numColsToDelete = ColumnsInStructure() - num_col_in_resized;

		//Now deleting the columns via pop-back
		for(int row=0; row < (int)Cells.size(); ++row)
		{
			int tcol = numColsToDelete;
			while(tcol)
			{
				Cells[row].pop_back();
				tcol--;
			}
		}

		Rows = Cells.size();

		if(Rows != 0)
		{
			Columns = Cells[0].size();
		}else
		{
			Columns = 0;
		}

		if(Rows == 0 || Columns == 0)
		{
			throw std::runtime_error("CellMatrix::ResizeUponNonData : resized matrix is of zero size");
		}

	}

	void CellMatrix::PutNewBlankRowOnBottom(void)
	{
		int cols = this->ColumnsInStructure();
		CellMatrix newRowArr(1,cols);
		this->PushBottom(newRowArr);
	}

	bool CellMatrix::isEmpty(void) const
	{
		if(Rows == 0 || Columns == 0)
		{
			return true;
		}else
		{
			return false;
		}
	}

	bool CellMatrix::isNumeric(void) const
	{
		bool ret = true;

		int t_rows = RowsInStructure();
		int t_cols = ColumnsInStructure();

		for(int i=0; i<t_rows; ++i)
		{
			for(int j=0; j<t_cols; ++j)
			{
				if(this->operator()(i,j).IsANumber() == false)
				{
					ret = false;
				}
			}
		}

		return ret;
	}

	CellMatrix CellMatrix::join(const CellMatrix& arg)
	{
		size_t row_arg = arg.RowsInStructure();
		size_t col_arg = arg.ColumnsInStructure();
		size_t new_cols;

		if(arg.isEmpty() == true || this->isEmpty() == true)
		{
			throw std::runtime_error("join : one of the CellMatrix which is trying to be joined is empty");
		}

		if(row_arg != Rows)
		{
			throw std::runtime_error("join : the CellMatrix which is trying to be joined to the current CellMatrix does not have the same number of rows as the current CellMatrix");
		}

		new_cols = Columns + col_arg;

		CellMatrix ret(row_arg, new_cols);

		for(size_t i=0; i<row_arg; ++i)
		{
			for(size_t j=0; j<Columns; ++j)
			{
				ret(i,j) = (*this)(i,j);
			}
			for(size_t j = Columns; j<new_cols; ++j)
			{
				ret(i,j) = arg(i, j-Columns);
			}
		}

		return ret;
	}

	bool CellMatrix::isPresent(const CellValue& arg)
	{
		bool ret = false;

		for(size_t i=0; i<Rows && !ret ; ++i)
		{
			for(size_t j=0; j<Columns && !ret; ++j)
			{
				if((*this)(i,j) == arg)
				{
					ret = true;
				}
			}
		}

		return ret;
	}

	void CellMatrix::checkIncreasingOrder(void)
	{
		for(int i=0; i<int(Rows)-1; ++i)
		{
			if((*this)(i,0).NumericValue() > (*this)(i+1,0).NumericValue())
			{
				throw std::runtime_error("CellMatrix : first column not in increasing order as one goes down the column.");
			}
		}
	}

	bool CellMatrix::checkDecreasingOrder(void)
	{
		bool ret = true;
		for(int i=0; i<int(Rows)-1; ++i)
		{
			if((*this)(i,0).NumericValue() < (*this)(i+1,0).NumericValue())
			{
				ret = false;
				break;
			}
		}
		return ret;
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	This assumes that the CellMatrix contains key-pairs. The 
	first column is assumed to be a string identitying the 
	field and the second column is the value. Note that the 
	second column can either be a string or a number
	*/
	CellValue CellMatrix::getValue(const std::string& field){
		int i; std::string lower_field; int arr_rows;
		lower_field = lowerCase(field);
		arr_rows = int(Rows);
		for(i=0; i<arr_rows; ++i){
			if(Cells[i][0].IsAString() == true || Cells[i][0].IsAWstring() == true){
				if(lowerCase(Cells[i][0].StringValue()) == lower_field){
					return Cells[i][1];
				}
			}
		}
		throw std::runtime_error("CellMatrix::getValue : field '"+field+"' not found");
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	Copy to

	This functon stores the contents of the matrix (within the CellMatrix)
	into the one dimension array pointed to by p. Note that p must already
	be allocated before it is used in this function

	Note that CellMatrix must only be populated with doubles
	*/
	void CellMatrix::GSL_Array1D_CopyTo(double* p){

		if(p == NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_CopyTo : pointer cannot be null");
		}

		int mat_rows = int(Rows);
		int mat_cols = int(Columns);
		int i, j;

		for(i=0; i<mat_rows; ++i){
			for(j=0; j<mat_cols; ++j){
				p[i*mat_cols +  j] = Cells[i][j].NumericValue();
			}
		}
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	Copy from

	Extracts the contents of p (which is a one dimensional array) 
	into the CellMatrix
	*/
	void CellMatrix::GSL_Array1D_CopyFrom(double* p){

		if(p == NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_CopyFrom : pointer cannot be null");
		}

		int mat_rows = int(Rows);
		int mat_cols = int(Columns);
		int i, j;

		for(i=0; i<mat_rows; ++i){
			for(j=0; j<mat_cols; ++j){
				Cells[i][j] = p[i*mat_cols +  j];
			}
		}
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	Allocate

	This function takes a NULL pointer p, and allocates memory such that 
	p points to a one dimensional array. Note that the number of entries 
	which p can store is equal to the number of entries in the CellMatrix
	*/
	void CellMatrix::GSL_Array1D_Allocate(double*& p){

		if(p != NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_Allocate : array must be null");
		}

		int mat_rows = int(Rows);
		int mat_cols = int(Columns);
		int size1DArray = mat_rows * mat_cols;

		if(size1DArray == 0){
			throw std::runtime_error("CellMatrix::GSL_Array1D_Allocate : both dimensions of the cellmatrix must be non-zero");	
		}

		try{
			p = new double [size1DArray];
		}catch(std::bad_alloc& arg){
			throw std::runtime_error("CellMatrix::GSL_Array1D_AlCopyTo :  unable to allocate memory");
		}	
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	Allocate

	This operation can only be performed by square matrices

	This function takes a NULL pointers diag, upper_sub_diag, lower_sub_diag, 
	and allocates memory such that these arrays points to a one dimensional array. 
	Note that the number of entries which diag can store is equal to the number 
	of rows in the CellMatrix
	*/
	void CellMatrix::GSL_Array1D_TriAllocate(gsl_vector*& diag, gsl_vector*& upper_sub_diag, gsl_vector*& lower_sub_diag){
		if(diag != NULL || upper_sub_diag != NULL || lower_sub_diag != NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriAllocate : all arrays must be null");
		}

		int arr_rows = int(Rows);
		int arr_cols = int(Columns);

		if(arr_rows != arr_cols){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriAllocate : this operation is only valid for diagonal arrays"); 
		}

		if(arr_rows == 0){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriAllocate : Dimensions cannot be zero");
		}

		diag = gsl_vector_alloc(arr_rows);

		if(diag == 0){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriAllocate : error allocating memory for arrays");
		}		

		upper_sub_diag = gsl_vector_alloc(arr_rows-1);

		if(upper_sub_diag == 0){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriAllocate : error allocating memory for arrays");
		}

		lower_sub_diag = gsl_vector_alloc(arr_rows-1);

		if(lower_sub_diag == 0){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriAllocate : error allocating memory for arrays");
		}
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	void CellMatrix::GSL_Array1D_TriCopyTo(gsl_vector*& diag, gsl_vector*& upper_sub_diag, gsl_vector*& lower_sub_diag){
		int i, diag_size;
		double y_up, y, y_down;

		int arr_rows = int(Rows);
		int arr_cols = int(Columns);

		if(arr_rows != arr_cols){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriCopyTo : this operation is only valid for diagonal arrays"); 
		}

		if(diag == NULL || upper_sub_diag == NULL || lower_sub_diag == NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriCopyTo : all arrays must be null");
		}

		diag_size = arr_rows;

		for(i=0; i<diag_size; ++i){	
			y = Cells[i][i].NumericValue();
			gsl_vector_set(diag,i,y);
		}

		for(i=0; i<(diag_size-1); ++i){
			y_up = Cells[i][i+1].NumericValue();
			gsl_vector_set(upper_sub_diag,i,y_up);
		}

		for(i=0; i<(diag_size-1); ++i){
			y_down = Cells[i+1][i].NumericValue();
			gsl_vector_set(lower_sub_diag,i,y_down);
		}
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	void CellMatrix::GSL_Array1D_TriCopyFrom(gsl_vector*& diag, gsl_vector*& upper_sub_diag, gsl_vector*& lower_sub_diag){
		int i, diag_size;
		double y_up, y, y_down;

		if(diag == NULL || upper_sub_diag == NULL || lower_sub_diag == NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriCopyTo : all arrays must be null");
		}

		int arr_rows = int(Rows);
		int arr_cols = int(Columns);

		if(arr_rows != arr_cols){
			throw std::runtime_error("CellMatrix::GSL_Array1D_TriCopyTo : this operation is only valid for diagonal arrays"); 
		}

		diag_size = arr_rows;

		Cells[0][0] = gsl_vector_get(diag,0);

		for(i=1; i<diag_size; ++i){
			y = gsl_vector_get(diag,i);
			y_up = gsl_vector_get(upper_sub_diag, i-1);
			y_down = gsl_vector_get(lower_sub_diag, i-1);

			Cells[i-1][i] = y_up;
			Cells[i][i] = y;
			Cells[i+1][i] = y_down;
		}
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	Copy to

	This functon stores the contents of the matrix (within the CellMatrix)
	into the one dimension array pointed to by p. Note that p must already
	be allocated before it is used in this function

	Note that CellMatrix must only be populated with doubles
	*/
	void CellMatrix::GSL_Array1D_CopyTo(gsl_vector* p){

		if(p == NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_CopyTo : pointer cannot be null");
		}

		double val;
		int mat_rows = int(Rows);
		int mat_cols = int(Columns);
		int i, j;

		for(i=0; i<mat_rows; ++i){
			for(j=0; j<mat_cols; ++j){
				val = Cells[i][j].NumericValue();
				gsl_vector_set(p, i*mat_cols +  j,val);
			}
		}
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	Copy from

	Extracts the contents of p (which is a one dimensional array) 
	into the CellMatrix
	*/
	void CellMatrix::GSL_Array1D_CopyFrom(gsl_vector* p){

		if(p == NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_CopyFrom : pointer cannot be null");
		}

		double val;
		int mat_rows = int(Rows);
		int mat_cols = int(Columns);
		int i, j;

		for(i=0; i<mat_rows; ++i){
			for(j=0; j<mat_cols; ++j){
				val = gsl_vector_get(p, i*mat_cols +  j);
				Cells[i][j] = val;
			}
		}
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

	/*!
	Allocate

	This function takes a NULL pointer p, and allocates memory such that 
	p points to a one dimensional array. Note that the number of entries 
	which p can store is equal to the number of entries in the CellMatrix
	*/
	void CellMatrix::GSL_Array1D_Allocate(gsl_vector*& p){

		if(p != NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_Allocate : array must be null");
		}

		int mat_rows = int(Rows);
		int mat_cols = int(Columns);
		int size1DArray = mat_rows * mat_cols;

		if(size1DArray == 0){
			throw std::runtime_error("CellMatrix::GSL_Array1D_Allocate : both dimensions of the cellmatrix must be non-zero");	
		}

		p = gsl_vector_alloc(size1DArray);	

		if(p==NULL){
			throw std::runtime_error("CellMatrix::GSL_Array1D_AlCopyTo :  unable to allocate memory");
		}	
	}

	//*************************************************************************************
	//*************************************************************************************
	//*************************************************************************************

}//namespace xlw
