//
//
//                                  CellMatrix.h
//
//
/*
Copyright (C) 2006 Mark Joshi
Copyright (C) 2007, 2008 Eric Ehlers
Copyright (C) 2009 Narinder S Claire


This file is part of XLW, a free-software/open-source C++ wrapper of the
Excel C API - http://xlw.sourceforge.net/

XLW is free software: you can redistribute it and/or modify it under the
terms of the XLW license.  You should have received a copy of the
license along with this program; if not, please email xlw-users@lists.sf.net

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the license for more details.
*/

#ifndef CELL_MATRIX_H
#define CELL_MATRIX_H

#include <string>
#include <vector>
#include "MyContainers.h"
#include  <gsl_vector_double.h>

namespace xlw {

	class CellValue : clsXlw
	{

	public:
		bool IsAString() const;
		bool IsAWstring() const;
		bool IsANumber() const;
		bool IsBoolean() const;
		bool IsXlfOper() const;
		bool IsError() const;
		bool IsEmpty() const;

		CellValue(const std::string&);
		CellValue(const std::wstring&);
		CellValue(double Number);
		CellValue(unsigned long Code, bool Error=false); //Error = true if you want an error code
		CellValue(bool TrueFalse);
		CellValue(const char* values);
		CellValue(int i);

		CellValue();

		std::string StringValue() const;
		const std::wstring& WstringValue() const;
		const char* CharPtrValue() const;
		double NumericValue() const;
		bool BooleanValue() const;
		unsigned long ErrorValue() const;

		std::string StringValueLowerCase() const;
		std::wstring WstringValueLowerCase() const;

		enum ValueType
		{
			string, wstring, number, boolean, xlfoper, error, empty
		};

		int getType(void) const;

		operator std::string() const;
		operator std::wstring() const;
		operator bool() const;
		operator double() const;
		operator unsigned long() const;

		void clear();

		bool operator==(const CellValue& arg) const;

	private:
		ValueType Type;

		std::string ValueAsString;
		std::wstring ValueAsWstring;
		double ValueAsNumeric;
		bool ValueAsBool;
		unsigned long ValueAsErrorCode;

	};


	class CellMatrix : clsXlw
	{
	public:

		CellMatrix(size_t rows, size_t columns);
		CellMatrix();
		CellMatrix(double x);
		CellMatrix(std::string x);
		CellMatrix(std::wstring x);
		CellMatrix(const char* x);
		CellMatrix(const MyArray& data);
		CellMatrix(const MyMatrix& data);
		CellMatrix(unsigned long i);
		CellMatrix(int i);
		CellMatrix(const std::vector<CellValue>& i_Cells);
		CellMatrix(const std::vector<int>& data);

		const CellValue& operator()(size_t i, size_t j) const;
		CellValue& operator()(size_t i, size_t j);

		MyArray getAsVectorArray(void) const;
		dtdbpairvec getAsPairArray(void) const;

		size_t RowsInStructure() const;
		size_t ColumnsInStructure() const;

		std::vector<std::vector<CellValue> >::iterator begin();
		std::vector<std::vector<CellValue> >::iterator end();

		const std::vector<CellValue>& operator[](unsigned i) const;
		std::vector<CellValue>& operator[](unsigned i);

		//Note that this returns a column vector corresponding to 
		//the row vector of the CellMatrix
		const xlw::CellMatrix getCellMatrixRow(unsigned i) const;
		xlw::CellMatrix getCellMatrixRow(unsigned i);
		xlw::CellMatrix getCellMatrixFrom_dtdbpairvec(const dtdbpairvec& arg);

		void clear();

		//can be used as predicate functions. Note that the compares are based
		//on the first column, which is assumed to be an integer representing
		//the date
		static bool compareDataMatrixInts(const std::vector<CellValue>& a, 
			const std::vector<CellValue>& b);
		void sort(int col);
		static bool reverseCompareDataMatrix(const std::vector<CellValue>& a, 
			const std::vector<CellValue>& b);

		void pop_front();
		void pop_back();
		void push_front(const CellMatrix& arg);
		//This pushes a full cellmatrix to the bottom. Note 
		//I think that PushBottom can only push a row at bottom.
		void push_back(const CellMatrix& arg);
		void PushBottom(const CellMatrix& newRows);
		const CellMatrix GetTranspose(void) const;
		const CellMatrix RemoveCol(int i_col) const;
		const CellMatrix RemoveRow(int i_row) const;
		void ResizeUponNonData(void);//this goes through the elements and finds the 
		//first row x, and the first column y, upon which a non-string and non-numeric entry
		//is found. Then the cellmatrix is resized to be of the first x-1 rows and the first
		//y-1 columns

		//This puts a new blank row at the bottom of the existing structure
		void PutNewBlankRowOnBottom(void);
		bool isEmpty(void) const;
		bool isNumeric(void) const;
		CellMatrix join(const CellMatrix& arg);//This function takes the current cellmatrix, joins
		//the cellmatrix arg on the right hand side, and returns the result. Note that the 
		//cellmatrix arg must have the same number of rows as the current cellmatrix. 
		bool isPresent(const CellValue& arg);
		void checkIncreasingOrder(void);//This function checks that the data in the first column increases
		//as one goes down the array. The reason for this is that there was an error where originally
		//copying and pasting between excel sheets in seperate excel instances resulted in a formatting 
		//error for long dated maturities (when pasting as text). Note that this does not appear when 
		//one copies and pastes between excel sheets in the same excel instance.
		bool checkDecreasingOrder(void);//As above, but checks that dates are decreasing as one goes down 
		//array
		CellValue getValue(const std::string& field);
		void GSL_Array1D_Allocate(double*& p);
		void GSL_Array1D_CopyTo(double* p);
		void GSL_Array1D_CopyFrom(double* p);
		void GSL_Array1D_Allocate(gsl_vector*& p);
		void GSL_Array1D_CopyTo(gsl_vector* p);
		void GSL_Array1D_CopyFrom(gsl_vector* p);
		void GSL_Array1D_TriAllocate(gsl_vector*& diag, gsl_vector*& upper_sub_diag, gsl_vector*& lower_sub_diag);
		void GSL_Array1D_TriCopyTo(gsl_vector*& diag, gsl_vector*& upper_sub_diag, gsl_vector*& lower_sub_diag);
		void GSL_Array1D_TriCopyFrom(gsl_vector*& diag, gsl_vector*& upper_sub_diag, gsl_vector*& lower_sub_diag);
	private:
		static bool compareDataMatrixForCol(const std::vector<CellValue>& a, 
			const std::vector<CellValue>& b);
                static int colToCompare;
		//int colToCompare;
		std::vector<std::vector<CellValue> > Cells;
		size_t Rows;
		size_t Columns;
		std::string lowerCase(const std::string& arg);
	};

	//int CellMatrix::colToCompare = 0;

	CellMatrix MergeCellMatrices(const CellMatrix& Top, const CellMatrix& Bottom);

}

#endif
